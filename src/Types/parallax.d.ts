import { ReactNode } from "react";

export interface parallaxProps {
    filter?: "primary" | "rose" | "dark" | "info" | "success" | "warning" | "danger";
    className: string;
    children?: ReactNode;
    style: any;
    image: string;
    small: boolean;
}
declare const Parallax: React.SFC<parallaxProps>;

export default Parallax;