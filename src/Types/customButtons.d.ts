import { ButtonProps } from "@material-ui/core";
import { ReactNode } from "react";

type NewButtonProps = Omit<ButtonProps, "size" | "color">;

export interface customButtonsProps extends NewButtonProps {
    color?: "primary" | "secondary" | "info" | "success" | "warning" | "danger" | "rose" | "white" | "twitter" | "facebook" | "google" | "linkedin" | "pinterest" | "youtube" | "tumblr" | "github" | "behance" | "dribbble" | "reddit" | "instagram" | "transparent";
    round?: boolean
    children?: ReactNode;
    fullWidth?: boolean
    disabled?: boolean
    simple?: boolean
    size?: "sm" | "lg";
    block?: boolean
    link?: boolean
    justIcon?: boolean
    fileButton?: boolean
    className?: string;
}
declare const Button: React.SFC<customButtonsProps>;

export default Button;