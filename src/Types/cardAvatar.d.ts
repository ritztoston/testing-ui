import { ReactNode } from "react";

export interface cardAvatarProps {
    children: ReactNode;
    className?: string;
    plain?: boolean;
    profile?: boolean;
    testimonial?: boolean;
    testimonialFooter?: boolean;
}
declare const CardAvatar: React.SFC<cardAvatarProps>;

export default CardAvatar;