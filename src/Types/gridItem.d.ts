import { ReactNode } from "react";
import { GridProps } from "@material-ui/core";

export interface gridItemProps extends GridProps {
    children?: ReactNode;
    className?: string;
}
declare const GridItem: React.SFC<gridItemProps>;

export default GridItem;