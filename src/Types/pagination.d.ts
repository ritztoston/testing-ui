export interface paginationProps {
    pages: {
        active?: boolean;
        disabled?: boolean;
        text: number | string;
        onClick?: () => void;
    }[];
    color: "primary" | "info" | "success" | "warning" | "danger";
    className: string;
}
declare const Pagination: React.SFC<paginationProps>;

export default Pagination;