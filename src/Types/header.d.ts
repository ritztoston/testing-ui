import { ReactNode } from "react";

export interface headerProps {
    changeColorOnScroll?: {
        height: number;
        color: "primary" | "info" | "success" | "warning" | "danger" | "transparent" | "white" | "rose" | "dark";
    };
    color?: "primary" | "info" | "success" | "warning" | "danger" | "transparent" | "white" | "rose" | "dark";
    links: ReactNode;
    brand?: string;
    fixed?: boolean;
    absolute?: boolean;
}
declare const Header: React.SFC<headerProps>;

export default Header;