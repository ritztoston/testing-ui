export interface tableProps {
    tableHead?: string[];
    tableData?: any[];
    tableHeaderColor?: "warning" | "primary" | "danger" | "success" | "info" | "rose" | "gray";
    hover?: boolean;
    colorsColls?: any[];
    coloredColls?: number[];
    customCellClasses?: string[];
    customClassesForCells?: number[];
    striped?: boolean;
    tableShopping?: boolean;
    customHeadCellClasses?: string[];
    customHeadClassesForCells?: number[];
}
declare const Table: React.SFC<tableProps>;

export default Table;