export interface customLinearProgressProps {
    color?: "primary" | "warning" | "danger" | "success" | "info" | "rose" | "gray";
}
declare const CustomLinearProgress: React.SFC<customLinearProgressProps>;

export default CustomLinearProgress;