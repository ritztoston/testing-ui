import { ReactNode } from "react";
import { GridProps } from "@material-ui/core";

export interface gridContainerProps extends GridProps {
    children?: ReactNode;
    className?: string;
}
declare const GridContainer: React.SFC<gridContainerProps>;

export default GridContainer;