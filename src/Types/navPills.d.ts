import { ReactNode } from "react";

export interface navPillsProps {
    active?: number;
    tabs: {
        tabButton: string;
        tabIcon: any;
        tabContent: ReactNode
    }[];
    direction?: string;
    color?: "primary" | "warning" | "danger" | "success" | "info" | "rose";
    horizontal?: {
        tabsGrid: any;
        contentGrid: any;
    };
    alignCenter?: boolean;
}
declare const NavPills: React.SFC<navPillsProps>;

export default NavPills;