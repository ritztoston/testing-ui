import { ReactNode } from "react";

export interface typographyProps {
    children?: ReactNode;
}
declare const Typography: React.SFC<typographyProps>;

export default Typography;