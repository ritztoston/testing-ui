export interface customFileInputProps {
    multiple?: boolean;
    id?: string;
    endButton?: any;
    startButton?: any;
    inputProps?: any;
    formControlProps?: any;
}
declare const CustomFileInput: React.SFC<customFileInputProps>;

export default CustomFileInput;