import { ReactNode } from "react";

export interface cardFooterProps {
    className?: string;
    children?: ReactNode;
    plain?: boolean;
    profile?: boolean;
    pricing?: boolean;
    testimonial?: boolean;
}
declare const CardFooter: React.SFC<cardFooterProps>;

export default CardFooter;