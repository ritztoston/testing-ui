export interface headerLinkProps {
    dropdownHoverColor?: "dark" | "primary" | "info" | "success" | "warning" | "danger" | "rose";
}
declare const HeaderLinks: React.SFC<headerLinkProps>;

export default HeaderLinks;