import { ReactNode } from "react";

export interface instructionProps {
    title: ReactNode;
    text: ReactNode;
    image: string;
    className?: string;
    imageClassName?: string;
    imageAlt?: string;
}
declare const Instruction: React.SFC<instructionProps>;

export default Instruction;