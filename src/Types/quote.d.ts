import { ReactNode } from "react";

export interface quoteProps {
    text?: ReactNode;
    author?: ReactNode;
    authorClassName?: string;
    textClassName?: string;
}
declare const Quote: React.SFC<quoteProps>;

export default Quote;