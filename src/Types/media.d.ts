import { ReactNode } from "react";

export interface mediaProps {
    avatarLink?: string;
    avatar?: string;
    avatarAlt?: string;
    title: ReactNode;
    body: ReactNode;
    footer: ReactNode;
    innerMedias: any;
}
declare const Media: React.SFC<mediaProps>;

export default Media;