import { ReactNode } from "react";

export interface cardHeaderProps {
    className?: string;
    children?: ReactNode;
    color?: "warning" | "success" | "danger" | "info" | "primary" | "rose";
    plain?: boolean;
    image?: boolean;
    contact?: boolean;
    signup?: boolean;
    noShadow?: boolean;
}
declare const CardHeader: React.SFC<cardHeaderProps>;

export default CardHeader;