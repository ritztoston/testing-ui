import { ReactNode } from "react";
import { InputProps } from "@material-ui/core";

export interface customInputProps extends InputProps {
    formControlProps?: any;
    labelText?: ReactNode;
    id?: string;
    labelProps?: any;
    inputProps?: any;
    error?: boolean;
    white?: boolean;
    inputRootCustomClasses?: string;
    success?: boolean;
}

declare const CustomInput: React.SFC<customInputProps>;

export default CustomInput;