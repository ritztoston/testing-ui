import { ReactNode } from "react";

export interface cardProps {
    className?: string;
    children?: ReactNode;
    plain?: boolean;
    profile?: boolean;
    blog?: boolean;
    raised?: boolean;
    background?: boolean;
    pricing?: boolean;
    color?: "primary" | "info" | "success" | "warning" | "danger" | "rose" | "white";
    product?: boolean;
    testimonial?: boolean;
}
declare const Card: React.SFC<cardProps>;

export default Card;