import { ReactNode } from "react";

export interface footerProps {
    children?: ReactNode;
    content: ReactNode;
    theme?: "dark" | "white" | "transparent";
    big?: boolean;
    className?: string;
}
declare const Footer: React.SFC<footerProps>;

export default Footer;