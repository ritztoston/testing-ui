import { ReactNode } from "react";

export interface customTabsProps {
    headerColor?: "warning" | "success" | "danger" | "info" | "primary" | "rose";
    title?: string;
    tabs?: {
        tabName: string;
        tabIcon: any;
        tabContent: ReactNode;
    }[];
    rtlActive?: boolean;
    plainTabs?: boolean;
}
declare const CustomTabs: React.SFC<customTabsProps>;

export default CustomTabs;