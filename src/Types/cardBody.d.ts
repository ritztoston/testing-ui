import { ReactNode } from "react";

export interface cardBodyProps {
    className?: string;
    children?: ReactNode;
    background?: boolean;
    plain?: boolean;
    formHorizontal?: boolean;
    pricing?: boolean;
    signup?: boolean;
    color?: boolean;
}
declare const CardBody: React.SFC<cardBodyProps>;

export default CardBody;