export interface accordionProps {
    active?: number | number[];
    collapses: {
        title: string;
        content: string;
    }[];
    activeColor?: "primary" | "secondary" | "warning" | "danger" | "success" | "info" | "rose";
}
declare const Accordion: React.SFC<accordionProps>;

export default Accordion;