import { ReactNode } from "react";

export interface badgeProps {
    color?: "badge" | "gray" | "primary" | "warning" | "danger" | "success" | "info" | "rose";
    children?: ReactNode;
    className?: string;
}
declare const Badge: React.SFC<badgeProps>;

export default Badge;