import { ReactNode } from "react";

export interface infoAreaProps {
    icon: any;
    title: string;
    description: ReactNode;
    iconColor?: "primary" | "warning" | "danger" | "success" | "info" | "rose" | "gray";
    vertical?: boolean;
    className?: string;
}
declare const InfoArea: React.SFC<infoAreaProps>;

export default InfoArea;