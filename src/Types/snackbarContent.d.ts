export interface snackbarContentProps {
    icon: any;
    message: string;
    color?: "info" | "success" | "warning" | "danger" | "primary";
    close?: boolean;
}
declare const SnackbarContent: React.SFC<snackbarContentProps>;

export default SnackbarContent;