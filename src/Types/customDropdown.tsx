import { SelectProps } from "@material-ui/core";

export interface customDropdownProps extends SelectProps {
    hoverColor?: "dark" | "primary" | "info" | "success" | "warning" | "danger" | "rose";
    buttonText?: React.ReactNode;
    buttonIcon?: any;
    dropdownList: any[];
    buttonProps?: any;
    dropup?: boolean;
    dropdownHeader?: React.ReactNode;
    rtlActive?: boolean;
    caret?: boolean;
    dropPlacement?: "bottom" | "top" | "right" | "left" | "bottom-start" | "bottom-end" | "top-start" | "top-end" | "right-start" | "right-end" | "left-start" | "left-end";
    noLiPadding?: boolean;
    innerDropDown?: boolean;
    navDropdown?: boolean;
    onClick?: any;
}
declare const CustomDropdown: React.SFC<customDropdownProps>;

export default CustomDropdown;