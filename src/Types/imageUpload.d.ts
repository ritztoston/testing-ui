export interface imageUploadProps {
    avatar?: boolean;
    addButtonProps?: any;
    changeButtonProps?: any;
    removeButtonProps?: any;
}
declare const ImageUpload: React.SFC<imageUploadProps>;

export default ImageUpload;