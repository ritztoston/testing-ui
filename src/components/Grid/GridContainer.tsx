import * as React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import { gridContainerProps } from "src/Types/gridContainer";

const styles = {
    grid: {
        marginRight: "-15px",
        marginLeft: "-15px",
        width: "auto"
    }
};

const useStyles = makeStyles(styles);

export default function GridContainer(props: gridContainerProps) {
    const { children, className = "", ...rest } = props;
    const classes = useStyles();
    return (
        <Grid container {...rest} className={classes.grid + " " + className}>
            {children}
        </Grid>
    );
}
