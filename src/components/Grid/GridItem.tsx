import * as React from "react";
import { makeStyles, createStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import { gridItemProps } from "src/Types/gridItem";

const styles = createStyles({
    grid: {
        position: "relative",
        width: "100%",
        minHeight: "1px",
        paddingRight: "15px",
        paddingLeft: "15px"
    }
});

const useStyles = makeStyles(styles);

export default function GridItem(props: gridItemProps) {
    const { children, className, ...rest } = props;
    const classes = useStyles();
    return (
        <Grid item {...rest} className={classes.grid + " " + className}>
            {children}
        </Grid>
    );
}
