import * as React from "react";
import { makeStyles } from "@material-ui/core/styles";
import LinearProgress from "@material-ui/core/LinearProgress";
import customLinearProgressStyle from "../../assets/jss/material-kit-pro-react/components/customLinearProgressStyle";
import { customLinearProgressProps } from "src/Types/customLinearProgress";

const useStyles = makeStyles(customLinearProgressStyle);

export default function CustomLinearProgress(props: customLinearProgressProps) {
    const { color = "primary", ...rest } = props;
    const classes: any = useStyles();
    return (
        <LinearProgress
            {...rest}
            classes={{
                root: classes.root + " " + classes[color + "Background"],
                bar: classes.bar + " " + classes[color]
            }}
        />
    );
}
