import * as React from "react";
import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";
import Icon from "@material-ui/core/Icon";
import infoStyle from "../../assets/jss/material-kit-pro-react/components/infoAreaStyle";
import { infoAreaProps } from "src/Types/infoArea";

const useStyles = makeStyles(infoStyle);

export default function InfoArea(props: infoAreaProps) {
    const { title, description, iconColor = "gray", vertical, className = "" } = props;
    const classes: any = useStyles();
    const iconWrapper = classNames({
        [classes.iconWrapper]: true,
        [classes[iconColor]]: true,
        [classes.iconWrapperVertical]: vertical
    });
    const iconClasses = classNames({
        [classes.icon]: true,
        [classes.iconVertical]: vertical
    });
    const infoAreaClasses = classNames({
        [classes.infoArea]: true,
        [className]: className !== undefined
    });
    let icon = null;
    switch (typeof props.icon) {
        case "string":
            icon = <Icon className={iconClasses}>{props.icon}</Icon>;
            break;
        default:
            icon = <props.icon className={iconClasses} />;
            break;
    }
    return (
        <div className={infoAreaClasses}>
            <div className={iconWrapper}>{icon}</div>
            <div className={classes.descriptionWrapper}>
                <h4 className={classes.title}>{title}</h4>
                <div className={classes.description}>{description}</div>
            </div>
        </div>
    );
}
