/* eslint-disable */
import * as React from "react";
import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Favorite from "@material-ui/icons/Favorite";
import footerStyle from "../../assets/jss/material-kit-pro-react/components/footerStyle";

const useStyles = makeStyles(footerStyle);

export default function Footer(props: { children: any; content: any; theme: any; big: any; className: any; }) {
    const { children, content, theme = "white", big, className = "" } = props;
    const classes: any = useStyles();
    const themeType =
        theme === "transparent" || theme == undefined ? false : true;
    const footerClasses = classNames({
        [classes.footer]: true,
        [classes[theme]]: themeType,
        [classes.big]: big || children !== undefined,
        [className]: className !== undefined
    });
    const aClasses = classNames({
        [classes.a]: true
    });

    return (
        <footer className={footerClasses}>
            <div className={classes.container}>
                {children !== undefined ? (
                    <div>
                        <div className={classes.content}>{children}</div>
                        <hr />
                    </div>
                ) : (
                        " "
                    )}
                {content}
                <div className={classes.clearFix} />
            </div>
        </footer>
    );
}
