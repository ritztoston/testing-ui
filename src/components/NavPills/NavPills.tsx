import * as React from "react";
import classNames from "classnames";
import SwipeableViews from "react-swipeable-views";
import { makeStyles } from "@material-ui/core/styles";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import GridContainer from "../Grid/GridContainer.js";
import GridItem from "../Grid/GridItem.js";
import { navPillsProps } from "src/Types/navPills.js";
import navPillsStyle from "../../assets/jss/material-kit-pro-react/components/navPillsStyle.js";

const useStyles = makeStyles(navPillsStyle);

export default function NavPills(props: navPillsProps) {
    const [active, setActive] = React.useState(props.active = 0);
    const handleChange = (event: any, active: number) => {
        setActive(active);
    };
    const handleChangeIndex = (index: number) => {
        setActive(index);
    };
    const { tabs, direction, color = "primary", horizontal, alignCenter } = props;
    const classes: any = useStyles();
    const flexContainerClasses = classNames({
        [classes.flexContainer]: true,
        [classes.horizontalDisplay]: horizontal !== undefined
    });
    const tabButtons = (
        <Tabs
            classes={{
                root: classes.root,
                fixed: classes.fixed,
                flexContainer: flexContainerClasses,
                indicator: classes.displayNone
            }}
            value={active}
            onChange={handleChange}
            centered={alignCenter}
        >
            {tabs.map((prop, key) => {
                var icon: any = {};
                if (prop.tabIcon !== undefined) {
                    icon["icon"] = <prop.tabIcon className={classes.tabIcon} />;
                }
                const pillsClasses = classNames({
                    [classes.pills]: true,
                    [classes.horizontalPills]: horizontal !== undefined,
                    [classes.pillsWithIcons]: prop.tabIcon !== undefined
                });
                return (
                    <Tab
                        label={prop.tabButton}
                        key={key}
                        {...icon}
                        classes={{
                            root: pillsClasses,
                            label: classes.label,
                            selected: classes[color]
                        }}
                    />
                );
            })}
        </Tabs>
    );
    const tabContent = (
        <div className={classes.contentWrapper}>
            <SwipeableViews
                axis={direction === "rtl" ? "x-reverse" : "x"}
                index={active}
                onChangeIndex={handleChangeIndex}
            >
                {tabs.map((prop, key) => {
                    return (
                        <div className={classes.tabContent} key={key}>
                            {prop.tabContent}
                        </div>
                    );
                })}
            </SwipeableViews>
        </div>
    );
    return horizontal !== undefined ? (
        <GridContainer>
            <GridItem {...horizontal.tabsGrid}>{tabButtons}</GridItem>
            <GridItem {...horizontal.contentGrid}>{tabContent}</GridItem>
        </GridContainer>
    ) : (
            <div>
                {tabButtons}
                {tabContent}
            </div>
        );
}
