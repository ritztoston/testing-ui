import * as React from "react";
import { makeStyles } from "@material-ui/core/styles";
import mediaStyle from "../../assets/jss/material-kit-pro-react/components/mediaStyle";
import { mediaProps } from "src/Types/media";

const useStyles = makeStyles(mediaStyle);

export default function Media(props: mediaProps) {
    const {
        avatarLink = "#pablo",
        avatar,
        avatarAlt = "...",
        title,
        body,
        footer,
        innerMedias,
        ...rest
    } = props;
    const classes = useStyles();
    return (
        <div {...rest} className={classes.media}>
            <a href={avatarLink} className={classes.mediaLink}>
                <div className={classes.mediaAvatar}>
                    <img src={avatar} alt={avatarAlt} />
                </div>
            </a>
            <div className={classes.mediaBody}>
                {title !== undefined ? (
                    <h4 className={classes.mediaHeading}>{title}</h4>
                ) : null}
                {body}
                <div className={classes.mediaFooter}>{footer}</div>
                {innerMedias !== undefined
                    ? innerMedias.map((prop: any) => {
                        return prop;
                    })
                    : null}
            </div>
        </div>
    );
}