import * as React from "react";
import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";
import { cardProps } from "src/Types/card";
import cardStyle from "../../assets/jss/material-kit-pro-react/components/cardStyle";

const useStyles = makeStyles(cardStyle);

const Card = (props: cardProps) => {
    const {
        className = "",
        children,
        plain,
        profile,
        blog,
        raised,
        background,
        pricing,
        color,
        product,
        testimonial,
        ...rest
    } = props;
    const classes: any = useStyles();
    const finalColor = color !== undefined ? ({ [classes[color]]: color }) : null;
    const cardClasses = classNames({
        [classes.card]: true,
        [classes.cardPlain]: plain,
        [classes.cardProfile]: profile || testimonial,
        [classes.cardBlog]: blog,
        [classes.cardRaised]: raised,
        [classes.cardBackground]: background,
        [classes.cardPricingColor]:
            (pricing && color !== undefined) || (pricing && background !== undefined),
        [classes.cardPricing]: pricing,
        [classes.cardProduct]: product,
        [className]: className !== undefined,
        ...finalColor
    });
    return (
        <div className={cardClasses} {...rest}>
            {children}
        </div>
    );
}

export default Card;
