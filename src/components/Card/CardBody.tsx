import * as React from "react";
import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";
import cardBodyStyle from "../../assets/jss/material-kit-pro-react/components/cardBodyStyle";
import { cardBodyProps } from "src/Types/cardBody";

const useStyles = makeStyles(cardBodyStyle);

export default function CardBody(props: cardBodyProps) {
    const {
        className = "",
        children,
        background,
        plain,
        formHorizontal,
        pricing,
        signup,
        color,
        ...rest
    } = props;
    const classes = useStyles();
    const cardBodyClasses = classNames({
        [classes.cardBody]: true,
        [classes.cardBodyBackground]: background,
        [classes.cardBodyPlain]: plain,
        [classes.cardBodyFormHorizontal]: formHorizontal,
        [classes.cardPricing]: pricing,
        [classes.cardSignup]: signup,
        [classes.cardBodyColor]: color,
        [className]: className !== undefined
    });
    return (
        <div className={cardBodyClasses} {...rest}>
            {children}
        </div>
    );
}
