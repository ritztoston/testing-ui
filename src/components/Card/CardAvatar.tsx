import * as React from "react";
import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";
import { cardAvatarProps } from "src/Types/cardAvatar";
import cardAvatarStyle from "../../assets/jss/material-kit-pro-react/components/cardAvatarStyle";

const useStyles = makeStyles(cardAvatarStyle);

const CardAvatar = (props: cardAvatarProps) => {
    const {
        children,
        className = "",
        plain,
        profile,
        testimonial,
        testimonialFooter,
        ...rest
    } = props;
    const classes = useStyles();
    const cardAvatarClasses = classNames({
        [classes.cardAvatar]: true,
        [classes.cardAvatarProfile]: profile,
        [classes.cardAvatarPlain]: plain,
        [classes.cardAvatarTestimonial]: testimonial,
        [classes.cardAvatarTestimonialFooter]: testimonialFooter,
        [className]: className !== undefined
    });
    return (
        <div className={cardAvatarClasses} {...rest}>
            {children}
        </div>
    );
}

export default CardAvatar;
