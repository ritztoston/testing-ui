import * as React from "react";
import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";
import { cardFooterProps } from "src/Types/cardFooter";
import cardFooterStyle from "../../assets/jss/material-kit-pro-react/components/cardFooterStyle";

const useStyles = makeStyles(cardFooterStyle);

export default function CardFooter(props: cardFooterProps) {
    const {
        className = "",
        children,
        plain,
        profile,
        pricing,
        testimonial,
        ...rest
    } = props;
    const classes = useStyles();
    const cardFooterClasses = classNames({
        [classes.cardFooter]: true,
        [classes.cardFooterPlain]: plain,
        [classes.cardFooterProfile]: profile || testimonial,
        [classes.cardFooterPricing]: pricing,
        [classes.cardFooterTestimonial]: testimonial,
        [className]: className !== undefined
    });
    return (
        <div className={cardFooterClasses} {...rest}>
            {children}
        </div>
    );
}
