import * as React from "react";
import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";
import { cardHeaderProps } from "src/Types/cardHeader";
import cardHeaderStyle from "../../assets/jss/material-kit-pro-react/components/cardHeaderStyle";

const useStyles = makeStyles(cardHeaderStyle);

export default function CardHeader(props: cardHeaderProps) {
    const {
        className = "",
        children,
        color = "primary",
        plain,
        image,
        contact,
        signup,
        noShadow,
        ...rest
    } = props;
    const classes: any = useStyles();
    const cardHeaderClasses = classNames({
        [classes.cardHeader]: true,
        [classes[color + "CardHeader"]]: color,
        [classes.cardHeaderPlain]: plain,
        [classes.cardHeaderImage]: image,
        [classes.cardHeaderContact]: contact,
        [classes.cardHeaderSignup]: signup,
        [classes.noShadow]: noShadow,
        [className]: className !== undefined
    });
    return (
        <div className={cardHeaderClasses} {...rest}>
            {children}
        </div>
    );
}
