import * as React from "react";
import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";
import Button, { ButtonProps } from "@material-ui/core/Button";
import { customButtonsProps } from "src/Types/customButtons";
import buttonStyle from "../../assets/jss/material-kit-pro-react/components/customButtonsStyle";

const useStyles = makeStyles(buttonStyle);

const RegularButton = React.forwardRef((props: customButtonsProps, ref: any) => {
    const {
        color = "primary",
        round,
        children,
        fullWidth,
        disabled,
        simple,
        size = "sm",
        block,
        link,
        justIcon,
        fileButton,
        className = "",
        ...rest
    } = props;
    const classes = useStyles();
    const btnClasses = classNames({
        [classes.button]: true,
        [classes[size]]: size,
        [classes[color]]: color,
        [classes.round]: round,
        [classes.fullWidth]: fullWidth,
        [classes.disabled]: disabled,
        [classes.simple]: simple,
        [classes.block]: block,
        [classes.link]: link,
        [classes.justIcon]: justIcon,
        [classes.fileButton]: fileButton,
        [className]: className
    });
    return (
        <Button {...rest} ref={ref} className={btnClasses}>
            {children}
        </Button>
    );
});

export default RegularButton;
