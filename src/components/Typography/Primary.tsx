import * as React from "react";
import { makeStyles } from "@material-ui/core/styles";
import typographyStyle from "../../assets/jss/material-kit-pro-react/components/typographyStyle";
import { typographyProps } from "src/Types/typography";

const useStyles = makeStyles(typographyStyle);

export default function Primary(props: typographyProps) {
    const { children } = props;
    const classes = useStyles();
    return (
        <div className={classes.defaultFontStyle + " " + classes.primaryText}>
            {children}
        </div>
    );
}
