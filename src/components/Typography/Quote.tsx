import * as React from "react";
import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";
import typographyStyle from "../../assets/jss/material-kit-pro-react/components/typographyStyle";
import { quoteProps } from "src/Types/quote";

const useStyles = makeStyles(typographyStyle);

export default function Quote(props: quoteProps) {
    const { text, author, authorClassName = "", textClassName = "" } = props;
    const classes: any = useStyles();
    const quoteClasses = classNames(classes.defaultFontStyle, classes.quote);
    const quoteTextClasses = classNames({
        [classes.quoteText]: true,
        [textClassName]: textClassName !== undefined
    });
    const quoteAuthorClasses = classNames({
        [classes.quoteAuthor]: true,
        [authorClassName]: authorClassName !== undefined
    });
    return (
        <blockquote className={quoteClasses}>
            <p className={quoteTextClasses}>{text}</p>
            <small className={quoteAuthorClasses}>{author}</small>
        </blockquote>
    );
}
