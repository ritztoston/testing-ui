import * as React from "react";
import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import { paginationProps } from "src/Types/pagination";
import paginationStyle from "../../assets/jss/material-kit-pro-react/components/paginationStyle";

const useStyles = makeStyles(paginationStyle);

export default function Pagination(props: paginationProps) {
    const { pages, color, className } = props;
    const classes: any = useStyles();
    const paginationClasses = classNames(classes.pagination, className);
    return (
        <ul className={paginationClasses}>
            {pages.map((prop, key) => {
                const paginationLink = classNames({
                    [classes.paginationLink]: true,
                    [classes[color]]: prop.active,
                    [classes.disabled]: prop.disabled
                });
                return (
                    <li className={classes.paginationItem} key={key}>
                        {prop.onClick !== undefined ? (
                            <Button
                                onClick={prop.onClick}
                                className={paginationLink}
                                disabled={prop.disabled}
                            >
                                {prop.text}
                            </Button>
                        ) : (
                                <Button
                                    onClick={() => alert("you've clicked " + prop.text)}
                                    className={paginationLink}
                                    disabled={prop.disabled}
                                >
                                    {prop.text}
                                </Button>
                            )}
                    </li>
                );
            })}
        </ul>
    );
}
