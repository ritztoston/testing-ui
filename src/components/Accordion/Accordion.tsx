import * as React from "react";
import ExpandMore from "@material-ui/icons/ExpandMore";
import { ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails, makeStyles } from "@material-ui/core";
import { accordionProps } from "src/Types/accordion";
import accordionStyle from "../../assets/jss/material-kit-pro-react/components/accordionStyle";

const useStyles = makeStyles(accordionStyle);

const Accordion = (props: accordionProps) => {
    const [active, setActive] = React.useState(
        props.active === undefined
            ? [-1]
            : Array.isArray(props.active)
                ? props.active
                : [props.active]
    );
    const [single] = React.useState(
        Array.isArray(props.active)
            ? true
            : false
    );
    const handleChange = (panel: any) => () => {
        let newArray: number[];

        if (single && Array.isArray(props.active)) {
            if (active[0] === panel) {
                newArray = [];
            } else {
                newArray = [panel];
            }
        } else {
            if (active.indexOf(panel) === -1) {
                newArray = [...active, panel];
            } else {
                newArray = [...active];
                newArray.splice(active.indexOf(panel), 1);
            }
        }
        setActive(newArray);
    };
    const { collapses, activeColor } = props;
    const classes: any = useStyles();

    return (
        <div className={classes.root}>
            {collapses.map((prop: { title: string, content: string }, key: number) => {
                return (
                    <ExpansionPanel
                        expanded={active.indexOf(key) !== -1}
                        onChange={handleChange(key)}
                        key={key}
                        classes={{
                            root: classes.expansionPanel,
                            expanded: classes.expansionPanelExpanded
                        }}
                    >
                        <ExpansionPanelSummary
                            expandIcon={<ExpandMore />}
                            classes={{
                                root: `${classes.expansionPanelSummary} ${
                                    classes[activeColor + "ExpansionPanelSummary"]
                                    }`,
                                expanded: `${classes.expansionPanelSummaryExpaned} ${
                                    classes[activeColor + "ExpansionPanelSummaryExpaned"]
                                    }`,
                                content: classes["expansionPanelSummaryContent"],
                                expandIcon: classes["expansionPanelSummaryExpandIcon"]
                            }}
                        >
                            <h4
                                className={classes.title}
                            >
                                {prop.title}
                            </h4>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails
                            className={classes.expansionPanelDetails}
                        >
                            {prop.content}
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                );
            })}
        </div>
    );
}

export default Accordion;
