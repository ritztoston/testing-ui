import * as React from "react";
import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Card from "../Card/Card.js";
import CardBody from "../Card/CardBody.js";
import CardHeader from "../Card/CardHeader.js";
import customTabsStyle from "../../assets/jss/material-kit-pro-react/components/customTabStyle.js";
import { customTabsProps } from "src/Types/customTabs.js";

const useStyles = makeStyles(customTabsStyle);

export default function CustomTabs(props: customTabsProps) {
    const [value, setValue] = React.useState(0);
    const handleChange = (event: any, value: React.SetStateAction<number>) => {
        setValue(value);
    };
    const { headerColor, title, tabs = [], rtlActive, plainTabs } = props;
    const classes = useStyles();
    const cardTitle = classNames({
        [classes.cardTitle]: true,
        [classes.cardTitleRTL]: rtlActive
    });
    const tabsContainer = classNames({
        [classes.tabsContainer]: true,
        [classes.tabsContainerRTL]: rtlActive
    });
    return (
        <Card plain={plainTabs}>
            <CardHeader color={headerColor} plain={plainTabs}>
                {title !== undefined ? (
                    <div className={cardTitle}>{"title"}</div>
                ) : null}
                <Tabs
                    classes={{
                        root: classes.customTabsRoot,
                        flexContainer: tabsContainer,
                        indicator: classes.displayNone
                    }}
                    value={value}
                    onChange={handleChange}
                    textColor="inherit"
                >
                    {tabs.map((prop, key) => {
                        var icon = {};
                        if (prop.tabIcon !== undefined) {
                            icon = {
                                icon: <prop.tabIcon className={classes.tabIcon} />
                            };
                        } else {
                            icon = {};
                        }
                        return (
                            <Tab
                                key={key}
                                classes={{
                                    root: classes.customTabRoot,
                                    selected: classes.customTabSelected,
                                    wrapper: classes.customTabWrapper
                                }}
                                // icon={<prop.tabIcon className={tabIcon} />}
                                {...icon}
                                label={prop.tabName}
                            />
                        );
                    })}
                </Tabs>
            </CardHeader>
            {/* <CardHeader
          classes={{
            root: cardHeader,
            title: cardTitle,
            content: classes.cardHeaderContent,
            action: classes.cardHeaderAction
          }}
          title={title}
          action={

          }
        /> */}
            <CardBody>
                {tabs.map((prop, key) => {
                    if (key === value) {
                        return <div key={key}>{prop.tabContent}</div>;
                    }
                    return null;
                })}
            </CardBody>
        </Card>
    );
}
