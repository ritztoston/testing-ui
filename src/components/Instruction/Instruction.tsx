import * as React from "react";
import cx from "classnames";
import { makeStyles } from "@material-ui/core/styles";
import GridContainer from "../Grid/GridContainer";
import GridItem from "../Grid/GridItem";
import instructionStyle from "../../assets/jss/material-kit-pro-react/components/instructionStyle";
import { instructionProps } from "src/Types/instruction";

const useStyles = makeStyles(instructionStyle);

export default function Instruction(props: instructionProps) {
    const { title, text, image, className = "", imageClassName = "", imageAlt = "..." } = props;
    const classes: any = useStyles();
    const instructionClasses = cx({
        [classes.instruction]: true,
        [className]: className !== undefined
    });
    const pictureClasses = cx({
        [classes.picture]: true,
        [imageClassName]: imageClassName !== undefined
    });
    return (
        <div className={instructionClasses}>
            <GridContainer>
                <GridItem xs={12} sm={12} md={8}>
                    <strong>{title}</strong>
                    <p>{text}</p>
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                    <div className={pictureClasses}>
                        <img src={image} alt={imageAlt} className={classes.image} />
                    </div>
                </GridItem>
            </GridContainer>
        </div>
    );
}
