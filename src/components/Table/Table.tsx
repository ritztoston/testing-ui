import * as React from "react";
import cx from "classnames";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import tableStyle from "../../assets/jss/material-kit-pro-react/components/tableStyle";
import { tableProps } from "src/Types/table";

const useStyles = makeStyles(tableStyle);

export default function CustomTable(props: tableProps) {
    const {
        tableHead = [],
        tableData = [],
        tableHeaderColor = "primary",
        hover,
        colorsColls,
        coloredColls,
        customCellClasses,
        customClassesForCells,
        striped,
        tableShopping,
        customHeadCellClasses,
        customHeadClassesForCells
    } = props;
    const classes: any = useStyles();
    return (
        <div className={classes.tableResponsive}>
            <Table className={classes.table}>
                {tableHead !== undefined ? (
                    <TableHead className={classes[tableHeaderColor]}>
                        <TableRow className={classes.tableRow}>
                            {tableHead.map((prop, key) => {
                                const tableCellClasses =
                                    classes.tableHeadCell +
                                    " " +
                                    classes.tableCell +
                                    " " +
                                    (customHeadCellClasses && customHeadClassesForCells
                                        ? cx({
                                            [customHeadCellClasses[
                                                customHeadClassesForCells.indexOf(key)
                                            ]]: customHeadClassesForCells.indexOf(key) !== -1,
                                            [classes.tableShoppingHead]: tableShopping
                                        })
                                        : "");
                                return (
                                    <TableCell className={tableCellClasses} key={key}>
                                        {prop}
                                    </TableCell>
                                );
                            })}
                        </TableRow>
                    </TableHead>
                ) : null}
                <TableBody>
                    {tableData.map((prop, key) => {
                        var rowColor = "";
                        var rowColored = false;
                        if (prop.color !== undefined) {
                            rowColor = prop.color;
                            rowColored = true;
                            prop = prop.data;
                        }
                        const tableRowClasses = cx({
                            [classes.tableRowHover]: hover,
                            [classes[rowColor + "Row"]]: rowColored,
                            [classes.tableStripedRow]: striped && key % 2 === 0
                        });
                        if (prop.total) {
                            return (
                                <TableRow key={key} hover={hover} className={tableRowClasses}>
                                    <TableCell
                                        className={classes.tableCell}
                                        colSpan={prop.colspan}
                                    />
                                    <TableCell
                                        className={classes.tableCell + " " + classes.tableCellTotal}
                                    >
                                        Total
                                    </TableCell>
                                    <TableCell
                                        className={
                                            classes.tableCell + " " + classes.tableCellAmount
                                        }
                                    >
                                        {prop.amount}
                                    </TableCell>
                                    {tableHead.length - (prop.colspan - 0 + 2) > 0 ? (
                                        <TableCell
                                            className={classes.tableCell}
                                            colSpan={tableHead.length - (prop.colspan - 0 + 2)}
                                        />
                                    ) : null}
                                </TableRow>
                            );
                        }
                        if (prop.purchase) {
                            return (
                                <TableRow key={key} hover={hover} className={tableRowClasses}>
                                    <TableCell
                                        className={classes.tableCell}
                                        colSpan={prop.colspan}
                                    />
                                    <TableCell
                                        className={classes.tableCell + " " + classes.tableCellTotal}
                                    >
                                        Total
                                    </TableCell>
                                    <TableCell
                                        className={
                                            classes.tableCell + " " + classes.tableCellAmount
                                        }
                                    >
                                        {prop.amount}
                                    </TableCell>
                                    <TableCell
                                        className={classes.tableCell + " " + classes.right}
                                        colSpan={prop.col.colspan}
                                    >
                                        {prop.col.text}
                                    </TableCell>
                                </TableRow>
                            );
                        }
                        return (
                            <TableRow
                                key={key}
                                hover={hover}
                                className={classes.tableRow + " " + tableRowClasses}
                            >
                                {prop.map((prop: React.ReactNode, key: number) => {
                                    const tableCellClasses =
                                        classes.tableCell +
                                        " " +
                                        (colorsColls && coloredColls && customCellClasses && customClassesForCells
                                            ? cx({
                                                [classes[colorsColls[coloredColls.indexOf(key)]]]:
                                                    coloredColls.indexOf(key) !== -1,
                                                [customCellClasses[customClassesForCells.indexOf(key)]]:
                                                    customClassesForCells.indexOf(key) !== -1
                                            })
                                            : null);
                                    return (
                                        <TableCell className={tableCellClasses} key={key}>
                                            {prop}
                                        </TableCell>
                                    );
                                })}
                            </TableRow>
                        );
                    })}
                </TableBody>
            </Table>
        </div>
    );
}
