import * as React from "react";
import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";
import { badgeProps } from "../../Types/badge";
import badgeStyle from "../../assets/jss/material-kit-pro-react/components/badgeStyle";

const useStyles = makeStyles(badgeStyle);

const Badge = (props: badgeProps) => {
    const { color = "gray", children, className = "" } = props;

    const classes = useStyles();
    const badgeClasses = classNames({
        [classes.badge]: true,
        [classes[color]]: true,
        [className]: className !== undefined
    });
    return <span className={badgeClasses}>{children}</span>;
}

export default Badge;
