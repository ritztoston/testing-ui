import * as React from "react";
import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";
import { parallaxProps } from "src/Types/parallax";
import parallaxStyle from "../../assets/jss/material-kit-pro-react/components/parallaxStyle";

const useStyles = makeStyles(parallaxStyle);

export default function Parallax(props: parallaxProps) {
    let windowScrollTop;
    if (window.innerWidth >= 768) {
        windowScrollTop = window.pageYOffset / 3;
    } else {
        windowScrollTop = 0;
    }
    const [transform, setTransform] = React.useState(
        "translate3d(0," + windowScrollTop + "px,0)"
    );
    React.useEffect(() => {
        if (window.innerWidth >= 768) {
            window.addEventListener("scroll", resetTransform);
        }
        return function cleanup() {
            if (window.innerWidth >= 768) {
                window.removeEventListener("scroll", resetTransform);
            }
        };
    });
    const resetTransform = () => {
        var windowScrollTop = window.pageYOffset / 3;
        setTransform("translate3d(0," + windowScrollTop + "px,0)");
    };
    const { filter = "primary", className, children, style = {}, image, small } = props;
    const classes: any = useStyles();
    const parallaxClasses = classNames({
        [classes.parallax]: true,
        [classes[filter + "Color"]]: filter !== undefined,
        [classes.small]: small,
        [className]: className !== undefined
    });
    return (
        <div
            className={parallaxClasses}
            style={{
                ...style,
                backgroundImage: "url(" + image + ")",
                transform: transform
            }}
        >
            {children}
        </div>
    );
}
