import * as React from "react";
import { makeStyles } from "@material-ui/core/styles";
// core components
import CustomInput from "../CustomInput/CustomInput.js";
import Button from "../CustomButtons/Button.js";
import customFileInputStyle from "../../assets/jss/material-kit-pro-react/components/customFileInputStyle.js";
import { customFileInputProps } from "src/Types/customFileInput.js";

const useStyles = makeStyles(customFileInputStyle);

export default function CustomFileInput(props: customFileInputProps) {
    const [fileNames, setFileNames] = React.useState("");
    // eslint-disable-next-line
    const [files, setFiles] = React.useState(null);
    let hiddenFile = React.createRef<any>();
    const onFocus = (e: any) => {
        hiddenFile.current.click(e);
    };
    // eslint-disable-next-line
    const handleSubmit = (e: any) => {
        e.preventDefault();
        // files is the file/image uploaded
        // in this function you can save the image (files) on form submit
        // you have to call it yourself
    };
    const addFile = (e: any) => {
        let fileNames = "";
        let files = e.target.files;
        for (let i = 0; i < e.target.files.length; i++) {
            fileNames = fileNames + e.target.files[i].name;
            if (props.multiple && i !== e.target.files.length - 1) {
                fileNames = fileNames + ", ";
            }
        }
        setFiles(files);
        setFileNames(fileNames);
    };
    const {
        id,
        endButton,
        startButton,
        inputProps,
        formControlProps,
        multiple = false
    } = props;
    const classes = useStyles();
    if (inputProps && inputProps.type && inputProps.type === "file") {
        inputProps.type = "text";
    }
    let buttonStart;
    let buttonEnd;
    if (startButton) {
        buttonStart = (
            <Button {...startButton.buttonProps}>
                {startButton.icon !== undefined ? startButton.icon : null}
                {startButton.text !== undefined ? startButton.text : null}
            </Button>
        );
    }
    if (endButton) {
        buttonEnd = (
            <Button {...endButton.buttonProps}>
                {endButton.icon !== undefined ? endButton.icon : null}
                {endButton.text !== undefined ? endButton.text : null}
            </Button>
        );
    }
    return (
        <div className={classes.inputFileWrapper}>
            <input
                type="file"
                className={classes.inputFile}
                multiple={multiple}
                ref={hiddenFile}
                onChange={addFile}
            />
            <CustomInput
                id={id}
                formControlProps={{
                    ...formControlProps
                }}
                inputProps={{
                    ...inputProps,
                    onClick: onFocus,
                    value: fileNames,
                    endAdornment: buttonEnd,
                    startAdornment: buttonStart
                }}
            />
        </div>
    );
}
