import * as React from "react";
import Button from "../CustomButtons/Button.js";
import defaultImage from "../../assets/img/image_placeholder";
import defaultAvatar from "../../assets/img/placeholder";
import { imageUploadProps } from "src/Types/imageUpload.js";

export default function ImageUpload(props: imageUploadProps) {
    const [file, setFile] = React.useState(null);
    const [imagePreviewUrl, setImagePreviewUrl] = React.useState(
        props.avatar ? defaultAvatar : defaultImage
    );
    let fileInput = React.createRef<any>();
    const handleImageChange = (e: any) => {
        e.preventDefault();
        let reader: any = new FileReader();
        let file = e.target.files[0];
        reader.onloadend = () => {
            setFile(file);
            setImagePreviewUrl(reader.result);
        };
        reader.readAsDataURL(file);
    };
    // eslint-disable-next-line
    const handleSubmit = (e: any) => {
        e.preventDefault();
        // file is the file/image uploaded
        // in this function you can save the image (file) on form submit
        // you have to call it yourself
    };
    const handleClick = () => {
        fileInput.current.click();
    };
    const handleRemove = () => {
        setFile(null);
        setImagePreviewUrl(props.avatar ? defaultAvatar : defaultImage);
        fileInput.current.value = null;
    };
    let { avatar, addButtonProps, changeButtonProps, removeButtonProps } = props;
    return (
        <div className="fileinput text-center">
            <input type="file" onChange={handleImageChange} ref={fileInput} />
            <div className={"thumbnail" + (avatar ? " img-circle" : "")}>
                <img src={imagePreviewUrl} alt="..." />
            </div>
            <div>
                {file === null ? (
                    <Button {...addButtonProps} onClick={() => handleClick()}>
                        {avatar ? "Add Photo" : "Select image"}
                    </Button>
                ) : (
                        <span>
                            <Button {...changeButtonProps} onClick={() => handleClick()}>
                                Change
                            </Button>
                            {avatar ? <br /> : null}
                            <Button {...removeButtonProps} onClick={() => handleRemove()}>
                                <i className="fas fa-times" /> Remove
                            </Button>
                        </span>
                    )}
            </div>
        </div>
    );
}
