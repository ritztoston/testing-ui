"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const classnames_1 = require("classnames");
const styles_1 = require("@material-ui/core/styles");
const typographyStyle_1 = require("../../assets/jss/material-kit-pro-react/components/typographyStyle");
const useStyles = styles_1.makeStyles(typographyStyle_1.default);
function Quote(props) {
    const { text, author, authorClassName = "", textClassName = "" } = props;
    const classes = useStyles();
    const quoteClasses = classnames_1.default(classes.defaultFontStyle, classes.quote);
    const quoteTextClasses = classnames_1.default({
        [classes.quoteText]: true,
        [textClassName]: textClassName !== undefined
    });
    const quoteAuthorClasses = classnames_1.default({
        [classes.quoteAuthor]: true,
        [authorClassName]: authorClassName !== undefined
    });
    return (React.createElement("blockquote", { className: quoteClasses },
        React.createElement("p", { className: quoteTextClasses }, text),
        React.createElement("small", { className: quoteAuthorClasses }, author)));
}
exports.default = Quote;
//# sourceMappingURL=Quote.js.map