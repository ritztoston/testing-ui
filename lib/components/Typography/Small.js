"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const styles_1 = require("@material-ui/core/styles");
const typographyStyle_1 = require("../../assets/jss/material-kit-pro-react/components/typographyStyle");
const useStyles = styles_1.makeStyles(typographyStyle_1.default);
function Small(props) {
    const { children } = props;
    const classes = useStyles();
    return (React.createElement("div", { className: classes.defaultFontStyle + " " + classes.smallText }, children));
}
exports.default = Small;
//# sourceMappingURL=Small.js.map