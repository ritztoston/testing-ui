"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const styles_1 = require("@material-ui/core/styles");
const typographyStyle_1 = require("../../assets/jss/material-kit-pro-react/components/typographyStyle");
const useStyles = styles_1.makeStyles(typographyStyle_1.default);
function Success(props) {
    const { children } = props;
    const classes = useStyles();
    return (React.createElement("div", { className: classes.defaultFontStyle + " " + classes.successText }, children));
}
exports.default = Success;
//# sourceMappingURL=Success.js.map