"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const classnames_1 = require("classnames");
const styles_1 = require("@material-ui/core/styles");
const Tabs_1 = require("@material-ui/core/Tabs");
const Tab_1 = require("@material-ui/core/Tab");
const Card_js_1 = require("../Card/Card.js");
const CardBody_js_1 = require("../Card/CardBody.js");
const CardHeader_js_1 = require("../Card/CardHeader.js");
const customTabStyle_js_1 = require("../../assets/jss/material-kit-pro-react/components/customTabStyle.js");
const useStyles = styles_1.makeStyles(customTabStyle_js_1.default);
function CustomTabs(props) {
    const [value, setValue] = React.useState(0);
    const handleChange = (event, value) => {
        setValue(value);
    };
    const { headerColor, title, tabs = [], rtlActive, plainTabs } = props;
    const classes = useStyles();
    const cardTitle = classnames_1.default({
        [classes.cardTitle]: true,
        [classes.cardTitleRTL]: rtlActive
    });
    const tabsContainer = classnames_1.default({
        [classes.tabsContainer]: true,
        [classes.tabsContainerRTL]: rtlActive
    });
    return (React.createElement(Card_js_1.default, { plain: plainTabs },
        React.createElement(CardHeader_js_1.default, { color: headerColor, plain: plainTabs },
            title !== undefined ? (React.createElement("div", { className: cardTitle }, "title")) : null,
            React.createElement(Tabs_1.default, { classes: {
                    root: classes.customTabsRoot,
                    flexContainer: tabsContainer,
                    indicator: classes.displayNone
                }, value: value, onChange: handleChange, textColor: "inherit" }, tabs.map((prop, key) => {
                var icon = {};
                if (prop.tabIcon !== undefined) {
                    icon = {
                        icon: React.createElement(prop.tabIcon, { className: classes.tabIcon })
                    };
                }
                else {
                    icon = {};
                }
                return (React.createElement(Tab_1.default, Object.assign({ key: key, classes: {
                        root: classes.customTabRoot,
                        selected: classes.customTabSelected,
                        wrapper: classes.customTabWrapper
                    } }, icon, { label: prop.tabName })));
            }))),
        React.createElement(CardBody_js_1.default, null, tabs.map((prop, key) => {
            if (key === value) {
                return React.createElement("div", { key: key }, prop.tabContent);
            }
            return null;
        }))));
}
exports.default = CustomTabs;
//# sourceMappingURL=CustomTabs.js.map