"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const classnames_1 = require("classnames");
const react_swipeable_views_1 = require("react-swipeable-views");
const styles_1 = require("@material-ui/core/styles");
const Tab_1 = require("@material-ui/core/Tab");
const Tabs_1 = require("@material-ui/core/Tabs");
const GridContainer_js_1 = require("../Grid/GridContainer.js");
const GridItem_js_1 = require("../Grid/GridItem.js");
const navPillsStyle_js_1 = require("../../assets/jss/material-kit-pro-react/components/navPillsStyle.js");
const useStyles = styles_1.makeStyles(navPillsStyle_js_1.default);
function NavPills(props) {
    const [active, setActive] = React.useState(props.active = 0);
    const handleChange = (event, active) => {
        setActive(active);
    };
    const handleChangeIndex = (index) => {
        setActive(index);
    };
    const { tabs, direction, color = "primary", horizontal, alignCenter } = props;
    const classes = useStyles();
    const flexContainerClasses = classnames_1.default({
        [classes.flexContainer]: true,
        [classes.horizontalDisplay]: horizontal !== undefined
    });
    const tabButtons = (React.createElement(Tabs_1.default, { classes: {
            root: classes.root,
            fixed: classes.fixed,
            flexContainer: flexContainerClasses,
            indicator: classes.displayNone
        }, value: active, onChange: handleChange, centered: alignCenter }, tabs.map((prop, key) => {
        var icon = {};
        if (prop.tabIcon !== undefined) {
            icon["icon"] = React.createElement(prop.tabIcon, { className: classes.tabIcon });
        }
        const pillsClasses = classnames_1.default({
            [classes.pills]: true,
            [classes.horizontalPills]: horizontal !== undefined,
            [classes.pillsWithIcons]: prop.tabIcon !== undefined
        });
        return (React.createElement(Tab_1.default, Object.assign({ label: prop.tabButton, key: key }, icon, { classes: {
                root: pillsClasses,
                label: classes.label,
                selected: classes[color]
            } })));
    })));
    const tabContent = (React.createElement("div", { className: classes.contentWrapper },
        React.createElement(react_swipeable_views_1.default, { axis: direction === "rtl" ? "x-reverse" : "x", index: active, onChangeIndex: handleChangeIndex }, tabs.map((prop, key) => {
            return (React.createElement("div", { className: classes.tabContent, key: key }, prop.tabContent));
        }))));
    return horizontal !== undefined ? (React.createElement(GridContainer_js_1.default, null,
        React.createElement(GridItem_js_1.default, Object.assign({}, horizontal.tabsGrid), tabButtons),
        React.createElement(GridItem_js_1.default, Object.assign({}, horizontal.contentGrid), tabContent))) : (React.createElement("div", null,
        tabButtons,
        tabContent));
}
exports.default = NavPills;
//# sourceMappingURL=NavPills.js.map