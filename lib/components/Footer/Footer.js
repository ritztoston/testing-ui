"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const classnames_1 = require("classnames");
const styles_1 = require("@material-ui/core/styles");
const footerStyle_1 = require("../../assets/jss/material-kit-pro-react/components/footerStyle");
const useStyles = styles_1.makeStyles(footerStyle_1.default);
function Footer(props) {
    const { children, content, theme = "white", big, className = "" } = props;
    const classes = useStyles();
    const themeType = theme === "transparent" || theme == undefined ? false : true;
    const footerClasses = classnames_1.default({
        [classes.footer]: true,
        [classes[theme]]: themeType,
        [classes.big]: big || children !== undefined,
        [className]: className !== undefined
    });
    const aClasses = classnames_1.default({
        [classes.a]: true
    });
    return (React.createElement("footer", { className: footerClasses },
        React.createElement("div", { className: classes.container },
            children !== undefined ? (React.createElement("div", null,
                React.createElement("div", { className: classes.content }, children),
                React.createElement("hr", null))) : (" "),
            content,
            React.createElement("div", { className: classes.clearFix }))));
}
exports.default = Footer;
//# sourceMappingURL=Footer.js.map