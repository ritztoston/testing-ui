"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const ExpandMore_1 = require("@material-ui/icons/ExpandMore");
const core_1 = require("@material-ui/core");
const accordionStyle_1 = require("../../assets/jss/material-kit-pro-react/components/accordionStyle");
const useStyles = core_1.makeStyles(accordionStyle_1.default);
const Accordion = (props) => {
    const [active, setActive] = React.useState(props.active === undefined
        ? [-1]
        : Array.isArray(props.active)
            ? props.active
            : [props.active]);
    const [single] = React.useState(Array.isArray(props.active)
        ? true
        : false);
    const handleChange = (panel) => () => {
        let newArray;
        if (single && Array.isArray(props.active)) {
            if (active[0] === panel) {
                newArray = [];
            }
            else {
                newArray = [panel];
            }
        }
        else {
            if (active.indexOf(panel) === -1) {
                newArray = [...active, panel];
            }
            else {
                newArray = [...active];
                newArray.splice(active.indexOf(panel), 1);
            }
        }
        setActive(newArray);
    };
    const { collapses, activeColor } = props;
    const classes = useStyles();
    return (React.createElement("div", { className: classes.root }, collapses.map((prop, key) => {
        return (React.createElement(core_1.ExpansionPanel, { expanded: active.indexOf(key) !== -1, onChange: handleChange(key), key: key, classes: {
                root: classes.expansionPanel,
                expanded: classes.expansionPanelExpanded
            } },
            React.createElement(core_1.ExpansionPanelSummary, { expandIcon: React.createElement(ExpandMore_1.default, null), classes: {
                    root: `${classes.expansionPanelSummary} ${classes[activeColor + "ExpansionPanelSummary"]}`,
                    expanded: `${classes.expansionPanelSummaryExpaned} ${classes[activeColor + "ExpansionPanelSummaryExpaned"]}`,
                    content: classes["expansionPanelSummaryContent"],
                    expandIcon: classes["expansionPanelSummaryExpandIcon"]
                } },
                React.createElement("h4", { className: classes.title }, prop.title)),
            React.createElement(core_1.ExpansionPanelDetails, { className: classes.expansionPanelDetails }, prop.content)));
    })));
};
exports.default = Accordion;
//# sourceMappingURL=Accordion.js.map