"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const classnames_1 = require("classnames");
const styles_1 = require("@material-ui/core/styles");
const Icon_1 = require("@material-ui/core/Icon");
const infoAreaStyle_1 = require("../../assets/jss/material-kit-pro-react/components/infoAreaStyle");
const useStyles = styles_1.makeStyles(infoAreaStyle_1.default);
function InfoArea(props) {
    const { title, description, iconColor = "gray", vertical, className = "" } = props;
    const classes = useStyles();
    const iconWrapper = classnames_1.default({
        [classes.iconWrapper]: true,
        [classes[iconColor]]: true,
        [classes.iconWrapperVertical]: vertical
    });
    const iconClasses = classnames_1.default({
        [classes.icon]: true,
        [classes.iconVertical]: vertical
    });
    const infoAreaClasses = classnames_1.default({
        [classes.infoArea]: true,
        [className]: className !== undefined
    });
    let icon = null;
    switch (typeof props.icon) {
        case "string":
            icon = React.createElement(Icon_1.default, { className: iconClasses }, props.icon);
            break;
        default:
            icon = React.createElement(props.icon, { className: iconClasses });
            break;
    }
    return (React.createElement("div", { className: infoAreaClasses },
        React.createElement("div", { className: iconWrapper }, icon),
        React.createElement("div", { className: classes.descriptionWrapper },
            React.createElement("h4", { className: classes.title }, title),
            React.createElement("div", { className: classes.description }, description))));
}
exports.default = InfoArea;
//# sourceMappingURL=InfoArea.js.map