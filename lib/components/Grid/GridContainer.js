"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const styles_1 = require("@material-ui/core/styles");
const Grid_1 = require("@material-ui/core/Grid");
const styles = {
    grid: {
        marginRight: "-15px",
        marginLeft: "-15px",
        width: "auto"
    }
};
const useStyles = styles_1.makeStyles(styles);
function GridContainer(props) {
    const { children, className = "" } = props, rest = __rest(props, ["children", "className"]);
    const classes = useStyles();
    return (React.createElement(Grid_1.default, Object.assign({ container: true }, rest, { className: classes.grid + " " + className }), children));
}
exports.default = GridContainer;
//# sourceMappingURL=GridContainer.js.map