"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const classnames_1 = require("classnames");
const styles_1 = require("@material-ui/core/styles");
const Table_1 = require("@material-ui/core/Table");
const TableBody_1 = require("@material-ui/core/TableBody");
const TableCell_1 = require("@material-ui/core/TableCell");
const TableHead_1 = require("@material-ui/core/TableHead");
const TableRow_1 = require("@material-ui/core/TableRow");
const tableStyle_1 = require("../../assets/jss/material-kit-pro-react/components/tableStyle");
const useStyles = styles_1.makeStyles(tableStyle_1.default);
function CustomTable(props) {
    const { tableHead = [], tableData = [], tableHeaderColor = "primary", hover, colorsColls, coloredColls, customCellClasses, customClassesForCells, striped, tableShopping, customHeadCellClasses, customHeadClassesForCells } = props;
    const classes = useStyles();
    return (React.createElement("div", { className: classes.tableResponsive },
        React.createElement(Table_1.default, { className: classes.table },
            tableHead !== undefined ? (React.createElement(TableHead_1.default, { className: classes[tableHeaderColor] },
                React.createElement(TableRow_1.default, { className: classes.tableRow }, tableHead.map((prop, key) => {
                    const tableCellClasses = classes.tableHeadCell +
                        " " +
                        classes.tableCell +
                        " " +
                        (customHeadCellClasses && customHeadClassesForCells
                            ? classnames_1.default({
                                [customHeadCellClasses[customHeadClassesForCells.indexOf(key)]]: customHeadClassesForCells.indexOf(key) !== -1,
                                [classes.tableShoppingHead]: tableShopping
                            })
                            : "");
                    return (React.createElement(TableCell_1.default, { className: tableCellClasses, key: key }, prop));
                })))) : null,
            React.createElement(TableBody_1.default, null, tableData.map((prop, key) => {
                var rowColor = "";
                var rowColored = false;
                if (prop.color !== undefined) {
                    rowColor = prop.color;
                    rowColored = true;
                    prop = prop.data;
                }
                const tableRowClasses = classnames_1.default({
                    [classes.tableRowHover]: hover,
                    [classes[rowColor + "Row"]]: rowColored,
                    [classes.tableStripedRow]: striped && key % 2 === 0
                });
                if (prop.total) {
                    return (React.createElement(TableRow_1.default, { key: key, hover: hover, className: tableRowClasses },
                        React.createElement(TableCell_1.default, { className: classes.tableCell, colSpan: prop.colspan }),
                        React.createElement(TableCell_1.default, { className: classes.tableCell + " " + classes.tableCellTotal }, "Total"),
                        React.createElement(TableCell_1.default, { className: classes.tableCell + " " + classes.tableCellAmount }, prop.amount),
                        tableHead.length - (prop.colspan - 0 + 2) > 0 ? (React.createElement(TableCell_1.default, { className: classes.tableCell, colSpan: tableHead.length - (prop.colspan - 0 + 2) })) : null));
                }
                if (prop.purchase) {
                    return (React.createElement(TableRow_1.default, { key: key, hover: hover, className: tableRowClasses },
                        React.createElement(TableCell_1.default, { className: classes.tableCell, colSpan: prop.colspan }),
                        React.createElement(TableCell_1.default, { className: classes.tableCell + " " + classes.tableCellTotal }, "Total"),
                        React.createElement(TableCell_1.default, { className: classes.tableCell + " " + classes.tableCellAmount }, prop.amount),
                        React.createElement(TableCell_1.default, { className: classes.tableCell + " " + classes.right, colSpan: prop.col.colspan }, prop.col.text)));
                }
                return (React.createElement(TableRow_1.default, { key: key, hover: hover, className: classes.tableRow + " " + tableRowClasses }, prop.map((prop, key) => {
                    const tableCellClasses = classes.tableCell +
                        " " +
                        (colorsColls && coloredColls && customCellClasses && customClassesForCells
                            ? classnames_1.default({
                                [classes[colorsColls[coloredColls.indexOf(key)]]]: coloredColls.indexOf(key) !== -1,
                                [customCellClasses[customClassesForCells.indexOf(key)]]: customClassesForCells.indexOf(key) !== -1
                            })
                            : null);
                    return (React.createElement(TableCell_1.default, { className: tableCellClasses, key: key }, prop));
                })));
            })))));
}
exports.default = CustomTable;
//# sourceMappingURL=Table.js.map