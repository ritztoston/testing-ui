"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const classnames_1 = require("classnames");
const styles_1 = require("@material-ui/core/styles");
const badgeStyle_1 = require("../../assets/jss/material-kit-pro-react/components/badgeStyle");
const useStyles = styles_1.makeStyles(badgeStyle_1.default);
const Badge = (props) => {
    const { color = "gray", children, className = "" } = props;
    const classes = useStyles();
    const badgeClasses = classnames_1.default({
        [classes.badge]: true,
        [classes[color]]: true,
        [className]: className !== undefined
    });
    return React.createElement("span", { className: badgeClasses }, children);
};
exports.default = Badge;
//# sourceMappingURL=Badge.js.map