"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_router_dom_1 = require("react-router-dom");
const styles_1 = require("@material-ui/core/styles");
const List_1 = require("@material-ui/core/List");
const ListItem_1 = require("@material-ui/core/ListItem");
const Icon_1 = require("@material-ui/core/Icon");
const Apps_1 = require("@material-ui/icons/Apps");
const ShoppingCart_1 = require("@material-ui/icons/ShoppingCart");
const ViewDay_1 = require("@material-ui/icons/ViewDay");
const Dns_1 = require("@material-ui/icons/Dns");
const Build_1 = require("@material-ui/icons/Build");
const List_2 = require("@material-ui/icons/List");
const People_1 = require("@material-ui/icons/People");
const Assignment_1 = require("@material-ui/icons/Assignment");
const MonetizationOn_1 = require("@material-ui/icons/MonetizationOn");
const Chat_1 = require("@material-ui/icons/Chat");
const Call_1 = require("@material-ui/icons/Call");
const ViewCarousel_1 = require("@material-ui/icons/ViewCarousel");
const AccountBalance_1 = require("@material-ui/icons/AccountBalance");
const ArtTrack_1 = require("@material-ui/icons/ArtTrack");
const ViewQuilt_1 = require("@material-ui/icons/ViewQuilt");
const LocationOn_1 = require("@material-ui/icons/LocationOn");
const Fingerprint_1 = require("@material-ui/icons/Fingerprint");
const AttachMoney_1 = require("@material-ui/icons/AttachMoney");
const Store_1 = require("@material-ui/icons/Store");
const AccountCircle_1 = require("@material-ui/icons/AccountCircle");
const PersonAdd_1 = require("@material-ui/icons/PersonAdd");
const Layers_1 = require("@material-ui/icons/Layers");
const ShoppingBasket_1 = require("@material-ui/icons/ShoppingBasket");
const LineStyle_1 = require("@material-ui/icons/LineStyle");
const Error_1 = require("@material-ui/icons/Error");
const CustomDropdown_1 = require("../CustomDropdown/CustomDropdown");
const Button_1 = require("../CustomButtons/Button");
const headerlinkStyle_1 = require("../../assets/jss/material-kit-pro-react/components/headerlinkStyle");
const useStyles = styles_1.makeStyles(headerlinkStyle_1.default);
function HeaderLinks(props) {
    const easeInOutQuad = (t, b, c, d) => {
        t /= d / 2;
        if (t < 1)
            return (c / 2) * t * t + b;
        t--;
        return (-c / 2) * (t * (t - 2) - 1) + b;
    };
    const smoothScroll = (e, target) => {
        if (window.location.pathname === "/sections") {
            var isMobile = navigator.userAgent.match(/(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i);
            if (isMobile) {
            }
            else {
                e.preventDefault();
                var targetScroll = document.getElementById(target);
                scrollGo(document.documentElement, targetScroll ? targetScroll.offsetTop : 0, 1250);
            }
        }
    };
    const scrollGo = (element, to, duration) => {
        var start = element.scrollTop, change = to - start, currentTime = 0, increment = 20;
        var animateScroll = function () {
            currentTime += increment;
            var val = easeInOutQuad(currentTime, start, change, duration);
            element.scrollTop = val;
            if (currentTime < duration) {
                setTimeout(animateScroll, increment);
            }
        };
        animateScroll();
    };
    var onClickSections = {};
    const { dropdownHoverColor } = props;
    const classes = useStyles();
    return (React.createElement(List_1.default, { className: classes.list + " " + classes.mlAuto },
        React.createElement(ListItem_1.default, { className: classes.listItem },
            React.createElement(CustomDropdown_1.default, { noLiPadding: true, navDropdown: true, hoverColor: dropdownHoverColor, buttonText: "Components", buttonProps: {
                    className: classes.navLink,
                    color: "transparent"
                }, buttonIcon: Apps_1.default, dropdownList: [
                    React.createElement(react_router_dom_1.Link, { to: "/", className: classes.dropdownLink },
                        React.createElement(LineStyle_1.default, { className: classes.dropdownIcons }),
                        " Presentation Page"),
                    React.createElement(react_router_dom_1.Link, { to: "/components", className: classes.dropdownLink },
                        React.createElement(Layers_1.default, { className: classes.dropdownIcons }),
                        "All components"),
                    React.createElement("a", { href: "https://demos.creative-tim.com/material-kit-pro-react/#/documentation/tutorial?ref=mkpr-navbar", target: "_blank", className: classes.dropdownLink },
                        React.createElement(Icon_1.default, { className: classes.dropdownIcons }, "content_paste"),
                        "Documentation")
                ] })),
        React.createElement(ListItem_1.default, { className: classes.listItem },
            React.createElement(CustomDropdown_1.default, { noLiPadding: true, navDropdown: true, hoverColor: dropdownHoverColor, buttonText: "Sections", buttonProps: {
                    className: classes.navLink,
                    color: "transparent"
                }, buttonIcon: ViewDay_1.default, dropdownList: [
                    React.createElement(react_router_dom_1.Link, { to: "/sections#headers", className: classes.dropdownLink, onClick: e => smoothScroll(e, "headers") },
                        React.createElement(Dns_1.default, { className: classes.dropdownIcons }),
                        " Headers"),
                    React.createElement(react_router_dom_1.Link, { to: "/sections#features", className: classes.dropdownLink, onClick: e => smoothScroll(e, "features") },
                        React.createElement(Build_1.default, { className: classes.dropdownIcons }),
                        " Features"),
                    React.createElement(react_router_dom_1.Link, { to: "/sections#blogs", className: classes.dropdownLink, onClick: e => smoothScroll(e, "blogs") },
                        React.createElement(List_2.default, { className: classes.dropdownIcons }),
                        " Blogs"),
                    React.createElement(react_router_dom_1.Link, { to: "/sections#teams", className: classes.dropdownLink, onClick: e => smoothScroll(e, "teams") },
                        React.createElement(People_1.default, { className: classes.dropdownIcons }),
                        " Teams"),
                    React.createElement(react_router_dom_1.Link, { to: "/sections#projects", className: classes.dropdownLink, onClick: e => smoothScroll(e, "projects") },
                        React.createElement(Assignment_1.default, { className: classes.dropdownIcons }),
                        " Projects"),
                    React.createElement(react_router_dom_1.Link, { to: "/sections#pricing", className: classes.dropdownLink, onClick: e => smoothScroll(e, "pricing") },
                        React.createElement(MonetizationOn_1.default, { className: classes.dropdownIcons }),
                        " Pricing"),
                    React.createElement(react_router_dom_1.Link, { to: "/sections#testimonials", className: classes.dropdownLink, onClick: e => smoothScroll(e, "testimonials") },
                        React.createElement(Chat_1.default, { className: classes.dropdownIcons }),
                        " Testimonials"),
                    React.createElement(react_router_dom_1.Link, { to: "/sections#contacts", className: classes.dropdownLink, onClick: e => smoothScroll(e, "contacts") },
                        React.createElement(Call_1.default, { className: classes.dropdownIcons }),
                        " Contacts")
                ] })),
        React.createElement(ListItem_1.default, { className: classes.listItem },
            React.createElement(CustomDropdown_1.default, { noLiPadding: true, navDropdown: true, hoverColor: dropdownHoverColor, buttonText: "Examples", buttonProps: {
                    className: classes.navLink,
                    color: "transparent"
                }, buttonIcon: ViewCarousel_1.default, dropdownList: [
                    React.createElement(react_router_dom_1.Link, { to: "/about-us", className: classes.dropdownLink },
                        React.createElement(AccountBalance_1.default, { className: classes.dropdownIcons }),
                        " About Us"),
                    React.createElement(react_router_dom_1.Link, { to: "/blog-post", className: classes.dropdownLink },
                        React.createElement(ArtTrack_1.default, { className: classes.dropdownIcons }),
                        " Blog Post"),
                    React.createElement(react_router_dom_1.Link, { to: "/blog-posts", className: classes.dropdownLink },
                        React.createElement(ViewQuilt_1.default, { className: classes.dropdownIcons }),
                        " Blog Posts"),
                    React.createElement(react_router_dom_1.Link, { to: "/contact-us", className: classes.dropdownLink },
                        React.createElement(LocationOn_1.default, { className: classes.dropdownIcons }),
                        " Contact Us"),
                    React.createElement(react_router_dom_1.Link, { to: "/landing-page", className: classes.dropdownLink },
                        React.createElement(ViewDay_1.default, { className: classes.dropdownIcons }),
                        " Landing Page"),
                    React.createElement(react_router_dom_1.Link, { to: "/login-page", className: classes.dropdownLink },
                        React.createElement(Fingerprint_1.default, { className: classes.dropdownIcons }),
                        " Login Page"),
                    React.createElement(react_router_dom_1.Link, { to: "/pricing", className: classes.dropdownLink },
                        React.createElement(AttachMoney_1.default, { className: classes.dropdownIcons }),
                        " Pricing Page"),
                    React.createElement(react_router_dom_1.Link, { to: "/shopping-cart-page", className: classes.dropdownLink },
                        React.createElement(ShoppingBasket_1.default, { className: classes.dropdownIcons }),
                        " Shopping Cart"),
                    React.createElement(react_router_dom_1.Link, { to: "/ecommerce-page", className: classes.dropdownLink },
                        React.createElement(Store_1.default, { className: classes.dropdownIcons }),
                        " Ecommerce Page"),
                    React.createElement(react_router_dom_1.Link, { to: "/product-page", className: classes.dropdownLink },
                        React.createElement(ShoppingCart_1.default, { className: classes.dropdownIcons }),
                        " Product Page"),
                    React.createElement(react_router_dom_1.Link, { to: "/profile-page", className: classes.dropdownLink },
                        React.createElement(AccountCircle_1.default, { className: classes.dropdownIcons }),
                        " Profile Page"),
                    React.createElement(react_router_dom_1.Link, { to: "/signup-page", className: classes.dropdownLink },
                        React.createElement(PersonAdd_1.default, { className: classes.dropdownIcons }),
                        " Signup Page"),
                    React.createElement(react_router_dom_1.Link, { to: "/error-page", className: classes.dropdownLink },
                        React.createElement(Error_1.default, { className: classes.dropdownIcons }),
                        " Error Page")
                ] })),
        React.createElement(ListItem_1.default, { className: classes.listItem },
            React.createElement(Button_1.default, { href: "https://www.creative-tim.com/product/material-kit-pro-react?ref=mkpr-navbar", color: window.innerWidth < 960 ? "info" : "white", className: classes.navButton, round: true, size: "sm" },
                React.createElement(ShoppingCart_1.default, { className: classes.icons }),
                " buy now"))));
}
exports.default = HeaderLinks;
//# sourceMappingURL=HeaderLinks.js.map