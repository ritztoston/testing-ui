"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_router_dom_1 = require("react-router-dom");
const classnames_1 = require("classnames");
const styles_1 = require("@material-ui/core/styles");
const AppBar_1 = require("@material-ui/core/AppBar");
const Toolbar_1 = require("@material-ui/core/Toolbar");
const IconButton_1 = require("@material-ui/core/IconButton");
const Button_1 = require("@material-ui/core/Button");
const Hidden_1 = require("@material-ui/core/Hidden");
const Drawer_1 = require("@material-ui/core/Drawer");
const Menu_1 = require("@material-ui/icons/Menu");
const Close_1 = require("@material-ui/icons/Close");
const headerStyle_1 = require("../../assets/jss/material-kit-pro-react/components/headerStyle");
const useStyles = styles_1.makeStyles(headerStyle_1.default);
function Header(props) {
    const [mobileOpen, setMobileOpen] = React.useState(false);
    const classes = useStyles();
    React.useEffect(() => {
        if (props.changeColorOnScroll) {
            window.addEventListener("scroll", headerColorChange);
        }
        return function cleanup() {
            if (props.changeColorOnScroll) {
                window.removeEventListener("scroll", headerColorChange);
            }
        };
    });
    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };
    const headerColorChange = () => {
        const { color = "primary", changeColorOnScroll } = props;
        const windowsScrollTop = window.pageYOffset;
        if (changeColorOnScroll) {
            if (windowsScrollTop > changeColorOnScroll.height) {
                document.body
                    .getElementsByTagName("header")[0]
                    .classList.remove(classes[color]);
                document.body
                    .getElementsByTagName("header")[0]
                    .classList.add(classes[changeColorOnScroll.color]);
            }
            else {
                document.body
                    .getElementsByTagName("header")[0]
                    .classList.add(classes[color]);
                document.body
                    .getElementsByTagName("header")[0]
                    .classList.remove(classes[changeColorOnScroll.color]);
            }
        }
    };
    const { color = "white", links, brand, fixed, absolute } = props;
    const appBarClasses = classnames_1.default({
        [classes.appBar]: true,
        [classes[color]]: color,
        [classes.absolute]: absolute,
        [classes.fixed]: fixed
    });
    return (React.createElement(AppBar_1.default, { className: appBarClasses },
        React.createElement(Toolbar_1.default, { className: classes.container },
            React.createElement(Button_1.default, { className: classes.title },
                React.createElement(react_router_dom_1.Link, { to: "/" }, brand)),
            React.createElement(Hidden_1.default, { smDown: true, implementation: "css" },
                React.createElement("div", { className: classes.hidden },
                    React.createElement("div", { className: classes.collapse }, links))),
            React.createElement(Hidden_1.default, { mdUp: true },
                React.createElement(IconButton_1.default, { color: "inherit", "aria-label": "open drawer", onClick: handleDrawerToggle },
                    React.createElement(Menu_1.default, null)))),
        React.createElement(Hidden_1.default, { mdUp: true, implementation: "js" },
            React.createElement(Drawer_1.default, { variant: "temporary", anchor: "right", open: mobileOpen, classes: {
                    paper: classes.drawerPaper
                }, onClose: handleDrawerToggle },
                React.createElement(IconButton_1.default, { color: "inherit", "aria-label": "open drawer", onClick: handleDrawerToggle, className: classes.closeButtonDrawer },
                    React.createElement(Close_1.default, null)),
                React.createElement("div", { className: classes.appResponsive }, links)))));
}
exports.default = Header;
//# sourceMappingURL=Header.js.map