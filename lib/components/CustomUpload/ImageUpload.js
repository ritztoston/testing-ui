"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const Button_js_1 = require("../CustomButtons/Button.js");
const image_placeholder_1 = require("../../assets/img/image_placeholder");
const placeholder_1 = require("../../assets/img/placeholder");
function ImageUpload(props) {
    const [file, setFile] = React.useState(null);
    const [imagePreviewUrl, setImagePreviewUrl] = React.useState(props.avatar ? placeholder_1.default : image_placeholder_1.default);
    let fileInput = React.createRef();
    const handleImageChange = (e) => {
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];
        reader.onloadend = () => {
            setFile(file);
            setImagePreviewUrl(reader.result);
        };
        reader.readAsDataURL(file);
    };
    const handleSubmit = (e) => {
        e.preventDefault();
    };
    const handleClick = () => {
        fileInput.current.click();
    };
    const handleRemove = () => {
        setFile(null);
        setImagePreviewUrl(props.avatar ? placeholder_1.default : image_placeholder_1.default);
        fileInput.current.value = null;
    };
    let { avatar, addButtonProps, changeButtonProps, removeButtonProps } = props;
    return (React.createElement("div", { className: "fileinput text-center" },
        React.createElement("input", { type: "file", onChange: handleImageChange, ref: fileInput }),
        React.createElement("div", { className: "thumbnail" + (avatar ? " img-circle" : "") },
            React.createElement("img", { src: imagePreviewUrl, alt: "..." })),
        React.createElement("div", null, file === null ? (React.createElement(Button_js_1.default, Object.assign({}, addButtonProps, { onClick: () => handleClick() }), avatar ? "Add Photo" : "Select image")) : (React.createElement("span", null,
            React.createElement(Button_js_1.default, Object.assign({}, changeButtonProps, { onClick: () => handleClick() }), "Change"),
            avatar ? React.createElement("br", null) : null,
            React.createElement(Button_js_1.default, Object.assign({}, removeButtonProps, { onClick: () => handleRemove() }),
                React.createElement("i", { className: "fas fa-times" }),
                " Remove"))))));
}
exports.default = ImageUpload;
//# sourceMappingURL=ImageUpload.js.map