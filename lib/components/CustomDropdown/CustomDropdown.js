"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const classnames_1 = require("classnames");
const styles_1 = require("@material-ui/core/styles");
const MenuItem_1 = require("@material-ui/core/MenuItem");
const MenuList_1 = require("@material-ui/core/MenuList");
const ClickAwayListener_1 = require("@material-ui/core/ClickAwayListener");
const Paper_1 = require("@material-ui/core/Paper");
const Grow_1 = require("@material-ui/core/Grow");
const Divider_1 = require("@material-ui/core/Divider");
const Popper_1 = require("@material-ui/core/Popper");
const Button_1 = require("../CustomButtons/Button");
const customDropdownStyle_1 = require("../../assets/jss/material-kit-pro-react/components/customDropdownStyle");
const useStyles = styles_1.makeStyles(customDropdownStyle_1.default);
function CustomDropdown(props) {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const handleClick = (event) => {
        if (anchorEl && anchorEl.contains(event.target)) {
            setAnchorEl(null);
        }
        else {
            setAnchorEl(event.currentTarget);
        }
    };
    const handleClose = (event) => {
        if (anchorEl.contains(event.target)) {
            return;
        }
        setAnchorEl(null);
    };
    const handleCloseMenu = (param) => {
        setAnchorEl(null);
        if (props && props.onClick) {
            props.onClick(param);
        }
    };
    const { buttonText, buttonIcon, dropdownList, buttonProps, dropup = false, dropdownHeader, caret = true, hoverColor = "primary", dropPlacement, rtlActive, noLiPadding, innerDropDown, navDropdown } = props;
    const classes = useStyles();
    const caretClasses = classnames_1.default({
        [classes.caret]: true,
        [classes.caretDropup]: dropup && !anchorEl,
        [classes.caretActive]: Boolean(anchorEl) && !dropup,
        [classes.caretRTL]: rtlActive
    });
    const dropdownItem = classnames_1.default({
        [classes.dropdownItem]: true,
        [classes[hoverColor + "Hover"]]: true,
        [classes.noLiPadding]: noLiPadding,
        [classes.dropdownItemRTL]: rtlActive
    });
    const dropDownMenu = (React.createElement(MenuList_1.default, { role: "menu", className: classes.menuList },
        dropdownHeader !== undefined ? (React.createElement(MenuItem_1.default, { onClick: () => handleCloseMenu(dropdownHeader), className: classes.dropdownHeader }, dropdownHeader)) : null,
        dropdownList.map((prop, key) => {
            if (prop.divider) {
                return (React.createElement(Divider_1.default, { key: key, onClick: () => handleCloseMenu("divider"), className: classes.dropdownDividerItem }));
            }
            else if (prop.props !== undefined &&
                prop.props["data-ref"] === "multi") {
                return (React.createElement(MenuItem_1.default, { key: key, className: dropdownItem, style: { overflow: "visible", padding: 0 } }, prop));
            }
            return (React.createElement(MenuItem_1.default, { key: key, onClick: () => handleCloseMenu(prop), className: dropdownItem }, prop));
        })));
    return (React.createElement("div", { className: innerDropDown ? classes.innerManager : classes.manager },
        React.createElement("div", { className: buttonText !== undefined ? "" : classes.target },
            React.createElement(Button_1.default, Object.assign({ "aria-label": "Notifications", "aria-owns": anchorEl ? "menu-list" : null, "aria-haspopup": "true" }, buttonProps, { onClick: handleClick }),
                buttonIcon !== undefined ? (React.createElement(props.buttonIcon, { className: classes.buttonIcon })) : null,
                buttonText !== undefined ? buttonText : null,
                caret ? React.createElement("b", { className: caretClasses }) : null)),
        React.createElement(Popper_1.default, { open: Boolean(anchorEl), anchorEl: anchorEl, transition: true, disablePortal: true, placement: dropPlacement, className: classnames_1.default({
                [classes.popperClose]: !anchorEl,
                [classes.pooperResponsive]: true,
                [classes.pooperNav]: Boolean(anchorEl) && navDropdown
            }) }, () => (React.createElement(Grow_1.default, { in: Boolean(anchorEl), style: dropup
                ? { transformOrigin: "0 100% 0" }
                : { transformOrigin: "0 0 0" } },
            React.createElement(Paper_1.default, { className: classes.dropdown }, innerDropDown ? (dropDownMenu) : (React.createElement(ClickAwayListener_1.default, { onClickAway: handleClose }, dropDownMenu))))))));
}
exports.default = CustomDropdown;
//# sourceMappingURL=CustomDropdown.js.map