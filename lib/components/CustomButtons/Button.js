"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const classnames_1 = require("classnames");
const styles_1 = require("@material-ui/core/styles");
const Button_1 = require("@material-ui/core/Button");
const customButtonsStyle_1 = require("../../assets/jss/material-kit-pro-react/components/customButtonsStyle");
const useStyles = styles_1.makeStyles(customButtonsStyle_1.default);
const RegularButton = React.forwardRef((props, ref) => {
    const { color = "primary", round, children, fullWidth, disabled, simple, size = "sm", block, link, justIcon, fileButton, className = "" } = props, rest = __rest(props, ["color", "round", "children", "fullWidth", "disabled", "simple", "size", "block", "link", "justIcon", "fileButton", "className"]);
    const classes = useStyles();
    const btnClasses = classnames_1.default({
        [classes.button]: true,
        [classes[size]]: size,
        [classes[color]]: color,
        [classes.round]: round,
        [classes.fullWidth]: fullWidth,
        [classes.disabled]: disabled,
        [classes.simple]: simple,
        [classes.block]: block,
        [classes.link]: link,
        [classes.justIcon]: justIcon,
        [classes.fileButton]: fileButton,
        [className]: className
    });
    return (React.createElement(Button_1.default, Object.assign({}, rest, { ref: ref, className: btnClasses }), children));
});
exports.default = RegularButton;
//# sourceMappingURL=Button.js.map