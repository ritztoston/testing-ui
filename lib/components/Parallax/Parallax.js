"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const classnames_1 = require("classnames");
const styles_1 = require("@material-ui/core/styles");
const parallaxStyle_1 = require("../../assets/jss/material-kit-pro-react/components/parallaxStyle");
const useStyles = styles_1.makeStyles(parallaxStyle_1.default);
function Parallax(props) {
    let windowScrollTop;
    if (window.innerWidth >= 768) {
        windowScrollTop = window.pageYOffset / 3;
    }
    else {
        windowScrollTop = 0;
    }
    const [transform, setTransform] = React.useState("translate3d(0," + windowScrollTop + "px,0)");
    React.useEffect(() => {
        if (window.innerWidth >= 768) {
            window.addEventListener("scroll", resetTransform);
        }
        return function cleanup() {
            if (window.innerWidth >= 768) {
                window.removeEventListener("scroll", resetTransform);
            }
        };
    });
    const resetTransform = () => {
        var windowScrollTop = window.pageYOffset / 3;
        setTransform("translate3d(0," + windowScrollTop + "px,0)");
    };
    const { filter = "primary", className, children, style = {}, image, small } = props;
    const classes = useStyles();
    const parallaxClasses = classnames_1.default({
        [classes.parallax]: true,
        [classes[filter + "Color"]]: filter !== undefined,
        [classes.small]: small,
        [className]: className !== undefined
    });
    return (React.createElement("div", { className: parallaxClasses, style: Object.assign(Object.assign({}, style), { backgroundImage: "url(" + image + ")", transform: transform }) }, children));
}
exports.default = Parallax;
//# sourceMappingURL=Parallax.js.map