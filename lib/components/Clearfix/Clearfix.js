"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const styles_1 = require("@material-ui/core/styles");
const styles = {
    clearfix: {
        "&:after,&:before": {
            display: "table",
            content: '" "'
        },
        "&:after": {
            clear: "both"
        }
    }
};
const useStyles = styles_1.makeStyles(styles);
function Clearfix() {
    const classes = useStyles();
    return React.createElement("div", { className: classes.clearfix });
}
exports.default = Clearfix;
//# sourceMappingURL=Clearfix.js.map