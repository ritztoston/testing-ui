"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const classnames_1 = require("classnames");
const styles_1 = require("@material-ui/core/styles");
const cardHeaderStyle_1 = require("../../assets/jss/material-kit-pro-react/components/cardHeaderStyle");
const useStyles = styles_1.makeStyles(cardHeaderStyle_1.default);
function CardHeader(props) {
    const { className = "", children, color = "primary", plain, image, contact, signup, noShadow } = props, rest = __rest(props, ["className", "children", "color", "plain", "image", "contact", "signup", "noShadow"]);
    const classes = useStyles();
    const cardHeaderClasses = classnames_1.default({
        [classes.cardHeader]: true,
        [classes[color + "CardHeader"]]: color,
        [classes.cardHeaderPlain]: plain,
        [classes.cardHeaderImage]: image,
        [classes.cardHeaderContact]: contact,
        [classes.cardHeaderSignup]: signup,
        [classes.noShadow]: noShadow,
        [className]: className !== undefined
    });
    return (React.createElement("div", Object.assign({ className: cardHeaderClasses }, rest), children));
}
exports.default = CardHeader;
//# sourceMappingURL=CardHeader.js.map