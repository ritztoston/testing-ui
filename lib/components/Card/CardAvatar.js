"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const classnames_1 = require("classnames");
const styles_1 = require("@material-ui/core/styles");
const cardAvatarStyle_1 = require("../../assets/jss/material-kit-pro-react/components/cardAvatarStyle");
const useStyles = styles_1.makeStyles(cardAvatarStyle_1.default);
const CardAvatar = (props) => {
    const { children, className = "", plain, profile, testimonial, testimonialFooter } = props, rest = __rest(props, ["children", "className", "plain", "profile", "testimonial", "testimonialFooter"]);
    const classes = useStyles();
    const cardAvatarClasses = classnames_1.default({
        [classes.cardAvatar]: true,
        [classes.cardAvatarProfile]: profile,
        [classes.cardAvatarPlain]: plain,
        [classes.cardAvatarTestimonial]: testimonial,
        [classes.cardAvatarTestimonialFooter]: testimonialFooter,
        [className]: className !== undefined
    });
    return (React.createElement("div", Object.assign({ className: cardAvatarClasses }, rest), children));
};
exports.default = CardAvatar;
//# sourceMappingURL=CardAvatar.js.map