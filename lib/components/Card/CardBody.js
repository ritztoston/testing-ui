"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const classnames_1 = require("classnames");
const styles_1 = require("@material-ui/core/styles");
const cardBodyStyle_1 = require("../../assets/jss/material-kit-pro-react/components/cardBodyStyle");
const useStyles = styles_1.makeStyles(cardBodyStyle_1.default);
function CardBody(props) {
    const { className = "", children, background, plain, formHorizontal, pricing, signup, color } = props, rest = __rest(props, ["className", "children", "background", "plain", "formHorizontal", "pricing", "signup", "color"]);
    const classes = useStyles();
    const cardBodyClasses = classnames_1.default({
        [classes.cardBody]: true,
        [classes.cardBodyBackground]: background,
        [classes.cardBodyPlain]: plain,
        [classes.cardBodyFormHorizontal]: formHorizontal,
        [classes.cardPricing]: pricing,
        [classes.cardSignup]: signup,
        [classes.cardBodyColor]: color,
        [className]: className !== undefined
    });
    return (React.createElement("div", Object.assign({ className: cardBodyClasses }, rest), children));
}
exports.default = CardBody;
//# sourceMappingURL=CardBody.js.map