"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const classnames_1 = require("classnames");
const styles_1 = require("@material-ui/core/styles");
const cardStyle_1 = require("../../assets/jss/material-kit-pro-react/components/cardStyle");
const useStyles = styles_1.makeStyles(cardStyle_1.default);
const Card = (props) => {
    const { className = "", children, plain, profile, blog, raised, background, pricing, color, product, testimonial } = props, rest = __rest(props, ["className", "children", "plain", "profile", "blog", "raised", "background", "pricing", "color", "product", "testimonial"]);
    const classes = useStyles();
    const finalColor = color !== undefined ? ({ [classes[color]]: color }) : null;
    const cardClasses = classnames_1.default(Object.assign({ [classes.card]: true, [classes.cardPlain]: plain, [classes.cardProfile]: profile || testimonial, [classes.cardBlog]: blog, [classes.cardRaised]: raised, [classes.cardBackground]: background, [classes.cardPricingColor]: (pricing && color !== undefined) || (pricing && background !== undefined), [classes.cardPricing]: pricing, [classes.cardProduct]: product, [className]: className !== undefined }, finalColor));
    return (React.createElement("div", Object.assign({ className: cardClasses }, rest), children));
};
exports.default = Card;
//# sourceMappingURL=Card.js.map