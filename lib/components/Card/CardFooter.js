"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const classnames_1 = require("classnames");
const styles_1 = require("@material-ui/core/styles");
const cardFooterStyle_1 = require("../../assets/jss/material-kit-pro-react/components/cardFooterStyle");
const useStyles = styles_1.makeStyles(cardFooterStyle_1.default);
function CardFooter(props) {
    const { className = "", children, plain, profile, pricing, testimonial } = props, rest = __rest(props, ["className", "children", "plain", "profile", "pricing", "testimonial"]);
    const classes = useStyles();
    const cardFooterClasses = classnames_1.default({
        [classes.cardFooter]: true,
        [classes.cardFooterPlain]: plain,
        [classes.cardFooterProfile]: profile || testimonial,
        [classes.cardFooterPricing]: pricing,
        [classes.cardFooterTestimonial]: testimonial,
        [className]: className !== undefined
    });
    return (React.createElement("div", Object.assign({ className: cardFooterClasses }, rest), children));
}
exports.default = CardFooter;
//# sourceMappingURL=CardFooter.js.map