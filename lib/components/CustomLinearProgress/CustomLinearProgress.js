"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const styles_1 = require("@material-ui/core/styles");
const LinearProgress_1 = require("@material-ui/core/LinearProgress");
const customLinearProgressStyle_1 = require("../../assets/jss/material-kit-pro-react/components/customLinearProgressStyle");
const useStyles = styles_1.makeStyles(customLinearProgressStyle_1.default);
function CustomLinearProgress(props) {
    const { color = "primary" } = props, rest = __rest(props, ["color"]);
    const classes = useStyles();
    return (React.createElement(LinearProgress_1.default, Object.assign({}, rest, { classes: {
            root: classes.root + " " + classes[color + "Background"],
            bar: classes.bar + " " + classes[color]
        } })));
}
exports.default = CustomLinearProgress;
//# sourceMappingURL=CustomLinearProgress.js.map