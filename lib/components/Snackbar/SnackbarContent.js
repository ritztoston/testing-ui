"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const styles_1 = require("@material-ui/core/styles");
const SnackbarContent_1 = require("@material-ui/core/SnackbarContent");
const IconButton_1 = require("@material-ui/core/IconButton");
const Icon_1 = require("@material-ui/core/Icon");
const Close_1 = require("@material-ui/icons/Close");
const snackbarContentStyle_1 = require("../../assets/jss/material-kit-pro-react/components/snackbarContentStyle");
const useStyles = styles_1.makeStyles(snackbarContentStyle_1.default);
function SnackbarContent(props) {
    const { message, color = "primary", close, icon } = props;
    const classes = useStyles();
    var action = [];
    const closeAlert = () => {
        setAlert(React.createElement(React.Fragment, null));
    };
    if (close !== undefined) {
        action = [
            React.createElement(IconButton_1.default, { className: classes.iconButton, key: "close", "aria-label": "Close", color: "inherit", onClick: closeAlert },
                React.createElement(Close_1.default, { className: classes.close }))
        ];
    }
    let snackIcon = null;
    switch (typeof icon) {
        case "object":
            snackIcon = React.createElement(props.icon, { className: classes.icon });
            break;
        case "string":
            snackIcon = React.createElement(Icon_1.default, { className: classes.icon }, props.icon);
            break;
        default:
            snackIcon = null;
            break;
    }
    const [alert, setAlert] = React.useState(React.createElement(SnackbarContent_1.default, { message: React.createElement("div", null,
            snackIcon,
            message,
            close !== undefined ? action : null), classes: {
            root: classes.root + " " + classes[color],
            message: classes.message + " " + classes.container
        } }));
    return alert;
}
exports.default = SnackbarContent;
//# sourceMappingURL=SnackbarContent.js.map