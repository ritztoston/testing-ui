"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const styles_1 = require("@material-ui/core/styles");
const mediaStyle_1 = require("../../assets/jss/material-kit-pro-react/components/mediaStyle");
const useStyles = styles_1.makeStyles(mediaStyle_1.default);
function Media(props) {
    const { avatarLink = "#pablo", avatar, avatarAlt = "...", title, body, footer, innerMedias } = props, rest = __rest(props, ["avatarLink", "avatar", "avatarAlt", "title", "body", "footer", "innerMedias"]);
    const classes = useStyles();
    return (React.createElement("div", Object.assign({}, rest, { className: classes.media }),
        React.createElement("a", { href: avatarLink, className: classes.mediaLink },
            React.createElement("div", { className: classes.mediaAvatar },
                React.createElement("img", { src: avatar, alt: avatarAlt }))),
        React.createElement("div", { className: classes.mediaBody },
            title !== undefined ? (React.createElement("h4", { className: classes.mediaHeading }, title)) : null,
            body,
            React.createElement("div", { className: classes.mediaFooter }, footer),
            innerMedias !== undefined
                ? innerMedias.map((prop) => {
                    return prop;
                })
                : null)));
}
exports.default = Media;
//# sourceMappingURL=Media.js.map