"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const styles_1 = require("@material-ui/core/styles");
const CustomInput_js_1 = require("../CustomInput/CustomInput.js");
const Button_js_1 = require("../CustomButtons/Button.js");
const customFileInputStyle_js_1 = require("../../assets/jss/material-kit-pro-react/components/customFileInputStyle.js");
const useStyles = styles_1.makeStyles(customFileInputStyle_js_1.default);
function CustomFileInput(props) {
    const [fileNames, setFileNames] = React.useState("");
    const [files, setFiles] = React.useState(null);
    let hiddenFile = React.createRef();
    const onFocus = (e) => {
        hiddenFile.current.click(e);
    };
    const handleSubmit = (e) => {
        e.preventDefault();
    };
    const addFile = (e) => {
        let fileNames = "";
        let files = e.target.files;
        for (let i = 0; i < e.target.files.length; i++) {
            fileNames = fileNames + e.target.files[i].name;
            if (props.multiple && i !== e.target.files.length - 1) {
                fileNames = fileNames + ", ";
            }
        }
        setFiles(files);
        setFileNames(fileNames);
    };
    const { id, endButton, startButton, inputProps, formControlProps, multiple = false } = props;
    const classes = useStyles();
    if (inputProps && inputProps.type && inputProps.type === "file") {
        inputProps.type = "text";
    }
    let buttonStart;
    let buttonEnd;
    if (startButton) {
        buttonStart = (React.createElement(Button_js_1.default, Object.assign({}, startButton.buttonProps),
            startButton.icon !== undefined ? startButton.icon : null,
            startButton.text !== undefined ? startButton.text : null));
    }
    if (endButton) {
        buttonEnd = (React.createElement(Button_js_1.default, Object.assign({}, endButton.buttonProps),
            endButton.icon !== undefined ? endButton.icon : null,
            endButton.text !== undefined ? endButton.text : null));
    }
    return (React.createElement("div", { className: classes.inputFileWrapper },
        React.createElement("input", { type: "file", className: classes.inputFile, multiple: multiple, ref: hiddenFile, onChange: addFile }),
        React.createElement(CustomInput_js_1.default, { id: id, formControlProps: Object.assign({}, formControlProps), inputProps: Object.assign(Object.assign({}, inputProps), { onClick: onFocus, value: fileNames, endAdornment: buttonEnd, startAdornment: buttonStart }) })));
}
exports.default = CustomFileInput;
//# sourceMappingURL=CustomFileInput.js.map