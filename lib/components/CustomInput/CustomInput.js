"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const classnames_1 = require("classnames");
const styles_1 = require("@material-ui/core/styles");
const FormControl_1 = require("@material-ui/core/FormControl");
const InputLabel_1 = require("@material-ui/core/InputLabel");
const Input_1 = require("@material-ui/core/Input");
const Clear_1 = require("@material-ui/icons/Clear");
const Check_1 = require("@material-ui/icons/Check");
const customInputStyle_1 = require("../../assets/jss/material-kit-pro-react/components/customInputStyle");
const useStyles = styles_1.makeStyles(customInputStyle_1.default);
function CustomInput(props) {
    const { formControlProps, labelText, id, labelProps, inputProps, error, white, inputRootCustomClasses = "", success } = props;
    const classes = useStyles();
    const labelClasses = classnames_1.default({
        [" " + classes.labelRootError]: error,
        [" " + classes.labelRootSuccess]: success && !error
    });
    const underlineClasses = classnames_1.default({
        [classes.underlineError]: error,
        [classes.underlineSuccess]: success && !error,
        [classes.underline]: true,
        [classes.whiteUnderline]: white
    });
    const marginTop = classnames_1.default({
        [inputRootCustomClasses]: inputRootCustomClasses !== undefined
    });
    const inputClasses = classnames_1.default({
        [classes.input]: true,
        [classes.whiteInput]: white
    });
    var formControlClasses;
    if (formControlProps !== undefined) {
        formControlClasses = classnames_1.default(formControlProps.className, classes.formControl);
    }
    else {
        formControlClasses = classes.formControl;
    }
    return (React.createElement(FormControl_1.default, Object.assign({}, formControlProps, { className: formControlClasses }),
        labelText !== undefined ? (React.createElement(InputLabel_1.default, Object.assign({ className: classes.labelRoot + " " + labelClasses, htmlFor: id }, labelProps), labelText)) : null,
        React.createElement(Input_1.default, Object.assign({ classes: {
                input: inputClasses,
                root: marginTop,
                disabled: classes.disabled,
                underline: underlineClasses
            }, id: id }, inputProps)),
        error ? (React.createElement(Clear_1.default, { className: classes.feedback + " " + classes.labelRootError })) : success ? (React.createElement(Check_1.default, { className: classes.feedback + " " + classes.labelRootSuccess })) : null));
}
exports.default = CustomInput;
//# sourceMappingURL=CustomInput.js.map