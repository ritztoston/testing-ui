"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const classnames_1 = require("classnames");
const styles_1 = require("@material-ui/core/styles");
const GridContainer_1 = require("../Grid/GridContainer");
const GridItem_1 = require("../Grid/GridItem");
const instructionStyle_1 = require("../../assets/jss/material-kit-pro-react/components/instructionStyle");
const useStyles = styles_1.makeStyles(instructionStyle_1.default);
function Instruction(props) {
    const { title, text, image, className = "", imageClassName = "", imageAlt = "..." } = props;
    const classes = useStyles();
    const instructionClasses = classnames_1.default({
        [classes.instruction]: true,
        [className]: className !== undefined
    });
    const pictureClasses = classnames_1.default({
        [classes.picture]: true,
        [imageClassName]: imageClassName !== undefined
    });
    return (React.createElement("div", { className: instructionClasses },
        React.createElement(GridContainer_1.default, null,
            React.createElement(GridItem_1.default, { xs: 12, sm: 12, md: 8 },
                React.createElement("strong", null, title),
                React.createElement("p", null, text)),
            React.createElement(GridItem_1.default, { xs: 12, sm: 12, md: 4 },
                React.createElement("div", { className: pictureClasses },
                    React.createElement("img", { src: image, alt: imageAlt, className: classes.image }))))));
}
exports.default = Instruction;
//# sourceMappingURL=Instruction.js.map