"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const classnames_1 = require("classnames");
const styles_1 = require("@material-ui/core/styles");
const Button_1 = require("@material-ui/core/Button");
const paginationStyle_1 = require("../../assets/jss/material-kit-pro-react/components/paginationStyle");
const useStyles = styles_1.makeStyles(paginationStyle_1.default);
function Pagination(props) {
    const { pages, color, className } = props;
    const classes = useStyles();
    const paginationClasses = classnames_1.default(classes.pagination, className);
    return (React.createElement("ul", { className: paginationClasses }, pages.map((prop, key) => {
        const paginationLink = classnames_1.default({
            [classes.paginationLink]: true,
            [classes[color]]: prop.active,
            [classes.disabled]: prop.disabled
        });
        return (React.createElement("li", { className: classes.paginationItem, key: key }, prop.onClick !== undefined ? (React.createElement(Button_1.default, { onClick: prop.onClick, className: paginationLink, disabled: prop.disabled }, prop.text)) : (React.createElement(Button_1.default, { onClick: () => alert("you've clicked " + prop.text), className: paginationLink, disabled: prop.disabled }, prop.text))));
    })));
}
exports.default = Pagination;
//# sourceMappingURL=Pagination.js.map