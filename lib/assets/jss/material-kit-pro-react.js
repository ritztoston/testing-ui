"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.hexToRgb = exports.coloredShadow = exports.btnLink = exports.mrAuto = exports.mlAuto = exports.sectionDescription = exports.sectionDark = exports.section = exports.mainRaised = exports.main = exports.cardSubtitle = exports.cardLink = exports.description = exports.cardTitle = exports.title = exports.defaultBoxShadow = exports.cardHeader = exports.cardActions = exports.roseCardHeader = exports.primaryCardHeader = exports.infoCardHeader = exports.dangerCardHeader = exports.successCardHeader = exports.warningCardHeader = exports.roseBoxShadow = exports.dangerBoxShadow = exports.warningBoxShadow = exports.successBoxShadow = exports.infoBoxShadow = exports.primaryBoxShadow = exports.instagramColor = exports.redditColor = exports.dribbbleColor = exports.behanceColor = exports.tumblrColor = exports.youtubeColor = exports.pinterestColor = exports.linkedinColor = exports.googleColor = exports.facebookColor = exports.twitterColor = exports.blackColor = exports.whiteColor = exports.grayColor = exports.roseColor = exports.infoColor = exports.successColor = exports.dangerColor = exports.warningColor = exports.secondaryColor = exports.primaryColor = exports.defaultFont = exports.card = exports.boxShadow = exports.containerFluid = exports.container = exports.transition = exports.drawerWidth = void 0;
const hexToRgb = (input) => {
    input = input + "";
    input = input.replace("#", "");
    let hexRegex = /[0-9A-Fa-f]/g;
    if (!hexRegex.test(input) || (input.length !== 3 && input.length !== 6)) {
        throw new Error("input is not a valid hex color.");
    }
    if (input.length === 3) {
        let first = input[0];
        let second = input[1];
        let last = input[2];
        input = first + first + second + second + last + last;
    }
    input = input.toUpperCase(input);
    let first = input[0] + input[1];
    let second = input[2] + input[3];
    let last = input[4] + input[5];
    return (parseInt(first, 16) +
        ", " +
        parseInt(second, 16) +
        ", " +
        parseInt(last, 16));
};
exports.hexToRgb = hexToRgb;
const drawerWidth = 260;
exports.drawerWidth = drawerWidth;
const primaryColor = [
    "#9c27b0",
    "#ab47bc",
    "#8e24aa",
    "#af2cc5",
    "#e1bee7",
    "#ba68c8"
];
exports.primaryColor = primaryColor;
const secondaryColor = ["#fafafa"];
exports.secondaryColor = secondaryColor;
const warningColor = [
    "#ff9800",
    "#ffa726",
    "#fb8c00",
    "#ffa21a",
    "#fcf8e3",
    "#faf2cc",
    "#ffe0b2",
    "#ffb74d"
];
exports.warningColor = warningColor;
const dangerColor = [
    "#f44336",
    "#ef5350",
    "#e53935",
    "#f55a4e",
    "#f2dede",
    "#ebcccc",
    "ef9a9a",
    "#ef5350"
];
exports.dangerColor = dangerColor;
const successColor = [
    "#4caf50",
    "#66bb6a",
    "#43a047",
    "#5cb860",
    "#dff0d8",
    "#d0e9c6",
    "#a5d6a7",
    "#66bb6a"
];
exports.successColor = successColor;
const infoColor = [
    "#00acc1",
    "#26c6da",
    "#00acc1",
    "#00d3ee",
    "#d9edf7",
    "#c4e3f3",
    "#b2ebf2",
    "#4dd0e1"
];
exports.infoColor = infoColor;
const roseColor = ["#e91e63", "#ec407a", "#d81b60", "#f8bbd0", "#f06292"];
exports.roseColor = roseColor;
const grayColor = [
    "#999",
    "#3C4858",
    "#eee",
    "#343434",
    "#585858",
    "#232323",
    "#ddd",
    "#6c757d",
    "#333",
    "#212121",
    "#777",
    "#D2D2D2",
    "#AAA",
    "#495057",
    "#e5e5e5",
    "#555",
    "#f9f9f9",
    "#ccc",
    "#444",
    "#f2f2f2",
    "#89229b",
    "#c0c1c2",
    "#9a9a9a",
    "#f5f5f5",
    "#505050",
    "#1f1f1f"
];
exports.grayColor = grayColor;
const whiteColor = "#FFF";
exports.whiteColor = whiteColor;
const blackColor = "#000";
exports.blackColor = blackColor;
const twitterColor = "#55acee";
exports.twitterColor = twitterColor;
const facebookColor = "#3b5998";
exports.facebookColor = facebookColor;
const googleColor = "#dd4b39";
exports.googleColor = googleColor;
const linkedinColor = "#0976b4";
exports.linkedinColor = linkedinColor;
const pinterestColor = "#cc2127";
exports.pinterestColor = pinterestColor;
const youtubeColor = "#e52d27";
exports.youtubeColor = youtubeColor;
const tumblrColor = "#35465c";
exports.tumblrColor = tumblrColor;
const behanceColor = "#1769ff";
exports.behanceColor = behanceColor;
const dribbbleColor = "#ea4c89";
exports.dribbbleColor = dribbbleColor;
const redditColor = "#ff4500";
exports.redditColor = redditColor;
const instagramColor = "#125688";
exports.instagramColor = instagramColor;
const transition = {
    transition: "all 0.33s cubic-bezier(0.685, 0.0473, 0.346, 1)"
};
exports.transition = transition;
const containerFluid = {
    paddingRight: "15px",
    paddingLeft: "15px",
    marginRight: "auto",
    marginLeft: "auto",
    width: "100%"
};
exports.containerFluid = containerFluid;
const container = Object.assign(Object.assign({}, containerFluid), { "@media (min-width: 576px)": {
        maxWidth: "540px"
    }, "@media (min-width: 768px)": {
        maxWidth: "720px"
    }, "@media (min-width: 992px)": {
        maxWidth: "960px"
    }, "@media (min-width: 1200px)": {
        maxWidth: "1140px"
    } });
exports.container = container;
const card = {
    display: "inline-block",
    position: "relative",
    width: "100%",
    margin: "25px 0",
    boxShadow: "0 1px 4px 0 rgba(" + hexToRgb(blackColor) + ", 0.14)",
    borderRadius: "3px",
    color: "rgba(" + hexToRgb(blackColor) + ", 0.87)",
    background: whiteColor
};
exports.card = card;
const defaultFont = {
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    fontWeight: 300,
    lineHeight: "1.5em"
};
exports.defaultFont = defaultFont;
const boxShadow = {
    boxShadow: "0 10px 30px -12px rgba(" +
        hexToRgb(blackColor) +
        ", 0.42), 0 4px 25px 0px rgba(" +
        hexToRgb(blackColor) +
        ", 0.12), 0 8px 10px -5px rgba(" +
        hexToRgb(blackColor) +
        ", 0.2)"
};
exports.boxShadow = boxShadow;
const primaryBoxShadow = {
    boxShadow: "0 12px 20px -10px rgba(" +
        hexToRgb(primaryColor[0]) +
        ", 0.28), 0 4px 20px 0px rgba(" +
        hexToRgb(blackColor) +
        ", 0.12), 0 7px 8px -5px rgba(" +
        hexToRgb(primaryColor[0]) +
        ", 0.2)"
};
exports.primaryBoxShadow = primaryBoxShadow;
const infoBoxShadow = {
    boxShadow: "0 12px 20px -10px rgba(" +
        hexToRgb(infoColor[0]) +
        ", 0.28), 0 4px 20px 0px rgba(" +
        hexToRgb(blackColor) +
        ", 0.12), 0 7px 8px -5px rgba(" +
        hexToRgb(infoColor[0]) +
        ", 0.2)"
};
exports.infoBoxShadow = infoBoxShadow;
const successBoxShadow = {
    boxShadow: "0 12px 20px -10px rgba(" +
        hexToRgb(successColor[0]) +
        ", 0.28), 0 4px 20px 0px rgba(" +
        hexToRgb(blackColor) +
        ", 0.12), 0 7px 8px -5px rgba(" +
        hexToRgb(successColor[0]) +
        ", 0.2)"
};
exports.successBoxShadow = successBoxShadow;
const warningBoxShadow = {
    boxShadow: "0 12px 20px -10px rgba(" +
        hexToRgb(warningColor[0]) +
        ", 0.28), 0 4px 20px 0px rgba(" +
        hexToRgb(blackColor) +
        ", 0.12), 0 7px 8px -5px rgba(" +
        hexToRgb(warningColor[0]) +
        ", 0.2)"
};
exports.warningBoxShadow = warningBoxShadow;
const dangerBoxShadow = {
    boxShadow: "0 12px 20px -10px rgba(" +
        hexToRgb(dangerColor[0]) +
        ", 0.28), 0 4px 20px 0px rgba(" +
        hexToRgb(blackColor) +
        ", 0.12), 0 7px 8px -5px rgba(" +
        hexToRgb(dangerColor[0]) +
        ", 0.2)"
};
exports.dangerBoxShadow = dangerBoxShadow;
const roseBoxShadow = {
    boxShadow: "0 4px 20px 0px rgba(" +
        hexToRgb(blackColor) +
        ", 0.14), 0 7px 10px -5px rgba(" +
        hexToRgb(roseColor[0]) +
        ", 0.4)"
};
exports.roseBoxShadow = roseBoxShadow;
const warningCardHeader = Object.assign({ color: whiteColor, background: "linear-gradient(60deg, " + warningColor[1] + ", " + warningColor[2] + ")" }, warningBoxShadow);
exports.warningCardHeader = warningCardHeader;
const successCardHeader = Object.assign({ color: whiteColor, background: "linear-gradient(60deg, " + successColor[1] + ", " + successColor[2] + ")" }, successBoxShadow);
exports.successCardHeader = successCardHeader;
const dangerCardHeader = Object.assign({ color: whiteColor, background: "linear-gradient(60deg, " + dangerColor[1] + ", " + dangerColor[2] + ")" }, dangerBoxShadow);
exports.dangerCardHeader = dangerCardHeader;
const infoCardHeader = Object.assign({ color: whiteColor, background: "linear-gradient(60deg, " + infoColor[1] + ", " + infoColor[2] + ")" }, infoBoxShadow);
exports.infoCardHeader = infoCardHeader;
const primaryCardHeader = Object.assign({ color: whiteColor, background: "linear-gradient(60deg, " + primaryColor[1] + ", " + primaryColor[2] + ")" }, primaryBoxShadow);
exports.primaryCardHeader = primaryCardHeader;
const roseCardHeader = Object.assign({ color: whiteColor, background: "linear-gradient(60deg, " + roseColor[1] + ", " + roseColor[2] + ")" }, roseBoxShadow);
exports.roseCardHeader = roseCardHeader;
const cardActions = Object.assign({ margin: "0 20px 10px", paddingTop: "10px", borderTop: "1px solid  " + grayColor[2], height: "auto" }, defaultFont);
exports.cardActions = cardActions;
const cardHeader = {
    margin: "-30px 15px 0",
    borderRadius: "3px",
    padding: "15px"
};
exports.cardHeader = cardHeader;
const defaultBoxShadow = {
    border: "0",
    borderRadius: "3px",
    boxShadow: "0 10px 20px -12px rgba(" +
        hexToRgb(blackColor) +
        ", 0.42), 0 3px 20px 0px rgba(" +
        hexToRgb(blackColor) +
        ", 0.12), 0 8px 10px -5px rgba(" +
        hexToRgb(blackColor) +
        ", 0.2)",
    padding: "10px 0",
    transition: "all 150ms ease 0s"
};
exports.defaultBoxShadow = defaultBoxShadow;
const title = {
    color: grayColor[1],
    textDecoration: "none",
    fontWeight: 700,
    marginTop: "30px",
    marginBottom: "25px",
    minHeight: "32px",
    fontFamily: `"Roboto Slab", "Times New Roman", serif`
};
exports.title = title;
const cardTitle = {
    "&, & a": Object.assign(Object.assign({}, title), { marginTop: ".625rem", marginBottom: "0.75rem", minHeight: "auto" })
};
exports.cardTitle = cardTitle;
const cardLink = {
    "& + $cardLink": {
        marginLeft: "1.25rem"
    }
};
exports.cardLink = cardLink;
const cardSubtitle = {
    marginBottom: "0",
    marginTop: "-.375rem"
};
exports.cardSubtitle = cardSubtitle;
const main = {
    background: whiteColor,
    position: "relative",
    zIndex: "3"
};
exports.main = main;
const mainRaised = {
    "@media (max-width: 576px)": {
        marginTop: "-30px"
    },
    "@media (max-width: 830px)": {
        marginLeft: "10px",
        marginRight: "10px"
    },
    margin: "-60px 30px 0px",
    borderRadius: "6px",
    boxShadow: "0 16px 24px 2px rgba(" +
        hexToRgb(blackColor) +
        ", 0.14), 0 6px 30px 5px rgba(" +
        hexToRgb(blackColor) +
        ", 0.12), 0 8px 10px -5px rgba(" +
        hexToRgb(blackColor) +
        ", 0.2)"
};
exports.mainRaised = mainRaised;
const section = {
    backgroundPosition: "50%",
    backgroundSize: "cover"
};
exports.section = section;
const sectionDark = {
    backgroundColor: grayColor[3],
    background: "radial-gradient(ellipse at center," +
        grayColor[4] +
        " 0," +
        grayColor[5] +
        " 100%)"
};
exports.sectionDark = sectionDark;
const sectionDescription = {
    marginTop: "130px"
};
exports.sectionDescription = sectionDescription;
const description = {
    color: grayColor[0]
};
exports.description = description;
const mlAuto = {
    marginLeft: "auto"
};
exports.mlAuto = mlAuto;
const mrAuto = {
    marginRight: "auto"
};
exports.mrAuto = mrAuto;
const btnLink = {
    backgroundColor: "transparent",
    boxShdow: "none",
    marginTop: "5px",
    marginBottom: "5px"
};
exports.btnLink = btnLink;
const coloredShadow = {
    "@media all and (-ms-high-contrast: none), (-ms-high-contrast: active)": {
        display: "none !important"
    },
    transform: "scale(0.94)",
    top: "12px",
    filter: "blur(12px)",
    position: "absolute",
    width: "100%",
    height: "100%",
    backgroundSize: "cover",
    zIndex: "-1",
    transition: "opacity .45s",
    opacity: "0"
};
exports.coloredShadow = coloredShadow;
//# sourceMappingURL=material-kit-pro-react.js.map