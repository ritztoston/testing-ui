"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@material-ui/core");
const material_kit_pro_react_1 = require("../../material-kit-pro-react");
const customInputStyle = core_1.createStyles({
    disabled: {
        "&:before": {
            backgroundColor: "transparent !important"
        }
    },
    underline: {
        "&:hover:not($disabled):before,&:before": {
            borderBottomColor: material_kit_pro_react_1.grayColor[11] + " !important",
            borderBottomWidth: "1px !important"
        },
        "&:after": {
            borderBottomColor: material_kit_pro_react_1.primaryColor[0]
        }
    },
    underlineError: {
        "&:after": {
            borderBottomColor: material_kit_pro_react_1.dangerColor[0]
        }
    },
    underlineSuccess: {
        "&:after": {
            borderBottomColor: material_kit_pro_react_1.successColor[0]
        }
    },
    labelRoot: Object.assign(Object.assign({}, material_kit_pro_react_1.defaultFont), { color: material_kit_pro_react_1.grayColor[12] + " !important", fontWeight: 400, fontSize: "14px", lineHeight: "1.42857", top: "10px", letterSpacing: "unset", "& + $underline": {
            marginTop: "0px"
        } }),
    labelRootError: {
        color: material_kit_pro_react_1.dangerColor[0] + " !important"
    },
    labelRootSuccess: {
        color: material_kit_pro_react_1.successColor[0] + " !important"
    },
    feedback: {
        position: "absolute",
        bottom: "4px",
        right: "0",
        zIndex: 2,
        display: "block",
        width: "24px",
        height: "24px",
        textAlign: "center",
        pointerEvents: "none"
    },
    formControl: {
        margin: "0 0 17px 0",
        paddingTop: "27px",
        position: "relative",
        "& svg,& .fab,& .far,& .fal,& .fas,& .material-icons": {
            color: material_kit_pro_react_1.grayColor[13]
        }
    },
    whiteUnderline: {
        "&:hover:not($disabled):before,&:before": {
            borderBottomColor: material_kit_pro_react_1.whiteColor
        },
        "&:after": {
            borderBottomColor: material_kit_pro_react_1.whiteColor
        }
    },
    input: {
        color: material_kit_pro_react_1.grayColor[13],
        height: "unset",
        "&,&::placeholder": {
            fontSize: "14px",
            fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
            fontWeight: "400",
            lineHeight: "1.42857",
            opacity: "1"
        },
        "&::placeholder": {
            color: material_kit_pro_react_1.grayColor[12]
        }
    },
    whiteInput: {
        "&,&::placeholder": {
            color: material_kit_pro_react_1.whiteColor,
            opacity: "1"
        }
    }
});
exports.default = customInputStyle;
//# sourceMappingURL=customInputStyle.js.map