"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@material-ui/core");
const material_kit_pro_react_1 = require("../../material-kit-pro-react");
const customLinearProgressStyle = core_1.createStyles({
    root: {
        height: "4px",
        marginBottom: "20px",
        overflow: "hidden"
    },
    bar: {
        height: "4px"
    },
    primary: {
        backgroundColor: material_kit_pro_react_1.primaryColor[0]
    },
    warning: {
        backgroundColor: material_kit_pro_react_1.warningColor[0]
    },
    danger: {
        backgroundColor: material_kit_pro_react_1.dangerColor[0]
    },
    success: {
        backgroundColor: material_kit_pro_react_1.successColor[0]
    },
    info: {
        backgroundColor: material_kit_pro_react_1.infoColor[0]
    },
    rose: {
        backgroundColor: material_kit_pro_react_1.roseColor[0]
    },
    gray: {
        backgroundColor: material_kit_pro_react_1.grayColor[0]
    },
    primaryBackground: {
        background: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.primaryColor[0]) + ", 0.2)"
    },
    warningBackground: {
        background: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.warningColor[0]) + ", 0.2)"
    },
    dangerBackground: {
        background: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.dangerColor[0]) + ", 0.2)"
    },
    successBackground: {
        background: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.successColor[0]) + ", 0.2)"
    },
    infoBackground: {
        background: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.infoColor[0]) + ", 0.2)"
    },
    roseBackground: {
        background: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.roseColor[0]) + ", 0.2)"
    },
    grayBackground: {
        background: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.grayColor[6]) + ", 0.2)"
    }
});
exports.default = customLinearProgressStyle;
//# sourceMappingURL=customLinearProgressStyle.js.map