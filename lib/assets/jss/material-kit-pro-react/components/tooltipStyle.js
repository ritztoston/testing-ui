"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@material-ui/core");
const material_kit_pro_react_1 = require("../../material-kit-pro-react");
const tooltipsStyle = core_1.createStyles({
    tooltip: {
        padding: "10px 15px",
        minWidth: "130px",
        color: material_kit_pro_react_1.whiteColor,
        lineHeight: "1.7em",
        background: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.grayColor[15]) + ",0.9)",
        border: "none",
        borderRadius: "3px",
        boxShadow: "0 8px 10px 1px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
            ", 0.14), 0 3px 14px 2px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
            ", 0.12), 0 5px 5px -3px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
            ", 0.2)",
        maxWidth: "200px",
        textAlign: "center",
        fontFamily: '"Helvetica Neue",Helvetica,Arial,sans-serif',
        fontSize: "0.875em",
        fontStyle: "normal",
        fontWeight: 400,
        textShadow: "none",
        textTransform: "none",
        letterSpacing: "normal",
        wordBreak: "normal",
        wordSpacing: "normal",
        wordWrap: "normal",
        whiteSpace: "normal",
        lineBreak: "auto"
    }
});
exports.default = tooltipsStyle;
//# sourceMappingURL=tooltipStyle.js.map