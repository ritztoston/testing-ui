"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const material_kit_pro_react_1 = require("../../material-kit-pro-react");
const core_1 = require("@material-ui/core");
const cardStyle = core_1.createStyles({
    card: {
        border: "0",
        marginBottom: "30px",
        marginTop: "30px",
        borderRadius: "6px",
        color: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) + ", 0.87)",
        background: material_kit_pro_react_1.whiteColor,
        width: "100%",
        boxShadow: "0 2px 2px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
            ", 0.14), 0 3px 1px -2px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
            ", 0.2), 0 1px 5px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
            ", 0.12)",
        position: "relative",
        display: "flex",
        flexDirection: "column",
        minWidth: "0",
        wordWrap: "break-word",
        fontSize: ".875rem",
        "@media all and (-ms-high-contrast: none), (-ms-high-contrast: active)": {
            display: "inline-block !important"
        }
    },
    cardPlain: {
        background: "transparent",
        boxShadow: "none"
    },
    cardProfile: {
        marginTop: "30px",
        textAlign: "center"
    },
    cardBlog: {
        marginTop: "60px"
    },
    cardRaised: {
        boxShadow: "0 16px 38px -12px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
            ", 0.56), 0 4px 25px 0px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
            ", 0.12), 0 8px 10px -5px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
            ", 0.2)"
    },
    cardBackground: {
        backgroundPosition: "50%",
        backgroundSize: "cover",
        textAlign: "center",
        color: material_kit_pro_react_1.whiteColor,
        "& h3": {
            color: material_kit_pro_react_1.whiteColor + " !important"
        },
        "& p": {
            color: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.whiteColor) + ",0.7)!important"
        },
        "&:after": {
            position: "absolute",
            zIndex: "1",
            width: "100%",
            height: "100%",
            display: "block",
            left: "0",
            top: "0",
            content: '""',
            backgroundColor: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) + ", 0.56)",
            borderRadius: "6px"
        },
        "& small": {
            color: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.whiteColor) + ", 0.7) !important"
        }
    },
    cardPricing: {
        textAlign: "center",
        "&:after": {
            backgroundColor: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) + ", 0.7) !important"
        },
        "& ul": {
            listStyle: "none",
            padding: 0,
            maxWidth: "240px",
            margin: "10px auto"
        },
        "& ul li": {
            color: material_kit_pro_react_1.grayColor[0],
            textAlign: "center",
            padding: "12px 0px",
            borderBottom: "1px solid rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.grayColor[0]) + ",0.3)"
        },
        "& ul li:last-child": {
            border: 0
        },
        "& ul li b": {
            color: material_kit_pro_react_1.grayColor[1]
        },
        "& h1": {
            marginTop: "30px"
        },
        "& h1 small": {
            display: "inline-flex",
            height: 0,
            fontSize: "18px"
        },
        "& h1 small:first-child": {
            position: "relative",
            top: "-17px",
            fontSize: "26px"
        },
        "& ul li svg,& ul li .fab,& ul li .fas,& ul li .far,& ul li .fal,& ul li .material-icons": {
            position: "relative",
            top: "7px"
        }
    },
    cardPricingColor: {
        "& ul li": {
            color: material_kit_pro_react_1.whiteColor,
            borderColor: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.whiteColor) + ",0.3)",
            "& b, & svg,& .fab,& .fas,& .far,& .fal,& .material-icons": {
                color: material_kit_pro_react_1.whiteColor,
                fontWeight: "700"
            }
        }
    },
    cardProduct: {
        marginTop: "30px"
    },
    primary: {
        background: "linear-gradient(60deg," + material_kit_pro_react_1.primaryColor[1] + ", " + material_kit_pro_react_1.primaryColor[2] + ")",
        "& h1 small": {
            color: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.whiteColor) + ", 0.8)"
        },
        color: material_kit_pro_react_1.whiteColor
    },
    info: {
        background: "linear-gradient(60deg," + material_kit_pro_react_1.infoColor[1] + "," + material_kit_pro_react_1.infoColor[2] + ")",
        "& h1 small": {
            color: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.whiteColor) + ", 0.8)"
        },
        color: material_kit_pro_react_1.whiteColor
    },
    success: {
        background: "linear-gradient(60deg," + material_kit_pro_react_1.successColor[1] + "," + material_kit_pro_react_1.successColor[2] + ")",
        "& h1 small": {
            color: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.whiteColor) + ", 0.8)"
        },
        color: material_kit_pro_react_1.whiteColor
    },
    warning: {
        background: "linear-gradient(60deg," + material_kit_pro_react_1.warningColor[1] + "," + material_kit_pro_react_1.warningColor[2] + ")",
        "& h1 small": {
            color: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.whiteColor) + ", 0.8)"
        },
        color: material_kit_pro_react_1.whiteColor
    },
    danger: {
        background: "linear-gradient(60deg," + material_kit_pro_react_1.dangerColor[1] + "," + material_kit_pro_react_1.dangerColor[2] + ")",
        "& h1 small": {
            color: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.whiteColor) + ", 0.8)"
        },
        color: material_kit_pro_react_1.whiteColor
    },
    rose: {
        background: "linear-gradient(60deg," + material_kit_pro_react_1.roseColor[1] + "," + material_kit_pro_react_1.roseColor[2] + ")",
        "& h1 small": {
            color: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.whiteColor) + ", 0.8)"
        },
        color: material_kit_pro_react_1.whiteColor
    }
});
exports.default = cardStyle;
//# sourceMappingURL=cardStyle.js.map