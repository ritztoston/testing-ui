"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@material-ui/core");
const material_kit_pro_react_1 = require("../../material-kit-pro-react");
const customDropdownStyle = (theme) => core_1.createStyles({
    popperClose: {
        pointerEvents: "none",
        display: "none !important"
    },
    pooperNav: {
        [theme.breakpoints.down("sm")]: {
            position: "static !important",
            left: "unset !important",
            top: "unset !important",
            transform: "none !important",
            willChange: "none !important",
            "& > div": {
                boxShadow: "none !important",
                marginLeft: "1.5rem",
                marginRight: "1.5rem",
                transition: "none !important",
                marginTop: "0px !important",
                marginBottom: "5px !important",
                padding: "0px !important"
            }
        }
    },
    manager: {
        "& > div > button:first-child > span:first-child, & > div > a:first-child > span:first-child": {
            width: "100%"
        }
    },
    innerManager: {
        display: "block",
        "& > div > button,& > div > a": {
            margin: "0px !important",
            color: "inherit !important",
            padding: "10px 20px !important",
            "& > span:first-child": {
                width: "100%",
                justifyContent: "flex-start"
            }
        }
    },
    target: {
        "& > button:first-child > span:first-child, & > a:first-child > span:first-child": {
            display: "inline-block"
        },
        "& $caret": {
            marginLeft: "0px"
        }
    },
    dropdown: {
        borderRadius: "3px",
        border: "0",
        boxShadow: "0 2px 5px 0 rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) + ", 0.26)",
        top: "100%",
        zIndex: 1000,
        minWidth: "160px",
        padding: "5px 0",
        margin: "2px 0 0",
        fontSize: "14px",
        textAlign: "left",
        listStyle: "none",
        backgroundColor: material_kit_pro_react_1.whiteColor,
        backgroundClip: "padding-box"
    },
    menuList: {
        padding: "0"
    },
    pooperResponsive: {
        zIndex: 1200,
        [theme.breakpoints.down("sm")]: {
            zIndex: "1640",
            position: "static",
            float: "none",
            width: "auto",
            marginTop: "0",
            backgroundColor: "transparent",
            border: "0",
            boxShadow: "none",
            color: "black"
        }
    },
    dropdownItem: Object.assign(Object.assign({}, material_kit_pro_react_1.defaultFont), { fontSize: "13px", padding: "10px 20px", margin: "0 5px", borderRadius: "2px", position: "relative", transition: "all 150ms linear", display: "block", clear: "both", fontWeight: 400, height: "100%", color: material_kit_pro_react_1.grayColor[8], whiteSpace: "nowrap", minHeight: "unset" }),
    darkHover: {
        "&:hover": {
            boxShadow: "0 4px 20px 0px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
                ", 0.14), 0 7px 10px -5px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.grayColor[9]) +
                ", 0.4)",
            backgroundColor: material_kit_pro_react_1.grayColor[9],
            color: material_kit_pro_react_1.whiteColor
        }
    },
    primaryHover: {
        "&:hover": Object.assign({ backgroundColor: material_kit_pro_react_1.primaryColor[0], color: material_kit_pro_react_1.whiteColor }, material_kit_pro_react_1.primaryBoxShadow)
    },
    infoHover: {
        "&:hover": Object.assign({ backgroundColor: material_kit_pro_react_1.infoColor[0], color: material_kit_pro_react_1.whiteColor }, material_kit_pro_react_1.infoBoxShadow)
    },
    successHover: {
        "&:hover": Object.assign({ backgroundColor: material_kit_pro_react_1.successColor[0], color: material_kit_pro_react_1.whiteColor }, material_kit_pro_react_1.successBoxShadow)
    },
    warningHover: {
        "&:hover": Object.assign({ backgroundColor: material_kit_pro_react_1.warningColor[0], color: material_kit_pro_react_1.whiteColor }, material_kit_pro_react_1.warningBoxShadow)
    },
    dangerHover: {
        "&:hover": Object.assign({ backgroundColor: material_kit_pro_react_1.dangerColor[0], color: material_kit_pro_react_1.whiteColor }, material_kit_pro_react_1.dangerBoxShadow)
    },
    roseHover: {
        "&:hover": Object.assign({ backgroundColor: material_kit_pro_react_1.roseColor[0], color: material_kit_pro_react_1.whiteColor }, material_kit_pro_react_1.roseBoxShadow)
    },
    dropdownItemRTL: {
        textAlign: "right"
    },
    dropdownDividerItem: {
        margin: "5px 0",
        backgroundColor: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) + ", 0.12)",
        height: "1px",
        overflow: "hidden"
    },
    buttonIcon: {
        width: "20px",
        height: "20px"
    },
    caret: {
        transition: "all 150ms ease-in",
        display: "inline-block",
        width: "0",
        height: "0",
        marginLeft: "4px",
        verticalAlign: "middle",
        borderTop: "4px solid",
        borderRight: "4px solid transparent",
        borderLeft: "4px solid transparent"
    },
    caretActive: {
        transform: "rotate(180deg)"
    },
    caretDropup: {
        transform: "rotate(180deg)"
    },
    caretRTL: {
        marginRight: "4px"
    },
    dropdownHeader: {
        display: "block",
        padding: "0.1875rem 1.25rem",
        fontSize: "0.75rem",
        lineHeight: "1.428571",
        color: material_kit_pro_react_1.grayColor[10],
        whiteSpace: "nowrap",
        fontWeight: "inherit",
        marginTop: "10px",
        minHeight: "24px",
        "&:hover,&:focus": {
            backgroundColor: "transparent",
            cursor: "auto"
        }
    },
    noLiPadding: {
        padding: "0"
    }
});
exports.default = customDropdownStyle;
//# sourceMappingURL=customDropdownStyle.js.map