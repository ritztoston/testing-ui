"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@material-ui/core");
const instructionStyle = core_1.createStyles({
    instruction: {},
    picture: {},
    image: {
        width: "100%",
        height: "auto",
        borderRadius: "6px",
        display: "block",
        maxWidth: "100%"
    }
});
exports.default = instructionStyle;
//# sourceMappingURL=instructionStyle.js.map