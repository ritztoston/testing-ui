"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@material-ui/core");
const material_kit_pro_react_1 = require("../../material-kit-pro-react");
const paginationStyle = core_1.createStyles({
    pagination: {
        display: "flex",
        paddingLeft: "0",
        listStyle: "none",
        borderRadius: "0.25rem"
    },
    paginationItem: {
        display: "inline"
    },
    paginationLink: {
        ":first-of-type": {
            marginleft: "0"
        },
        letterSpacing: "unset",
        border: "0",
        borderRadius: "30px !important",
        transition: "all .3s",
        padding: "0px 11px",
        margin: "0 3px",
        minWidth: "30px",
        height: "30px",
        minHeight: "auto",
        lineHeight: "30px",
        fontWeight: 400,
        fontSize: "12px",
        textTransform: "uppercase",
        background: "transparent",
        position: "relative",
        float: "left",
        textDecoration: "none",
        boxSizing: "border-box",
        "&,&:hover,&:focus": {
            color: material_kit_pro_react_1.grayColor[0]
        },
        "&:hover,&:focus": {
            zIndex: "3",
            backgroundColor: material_kit_pro_react_1.grayColor[2],
            borderColor: material_kit_pro_react_1.grayColor[6]
        },
        "&:hover": {
            cursor: "pointer"
        }
    },
    primary: {
        "&,&:hover,&:focus": {
            backgroundColor: material_kit_pro_react_1.primaryColor[0],
            borderColor: material_kit_pro_react_1.primaryColor[0],
            color: material_kit_pro_react_1.whiteColor,
            boxShadow: "0 4px 5px 0 rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.primaryColor[0]) +
                ", 0.14), 0 1px 10px 0 rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.primaryColor[0]) +
                ", 0.12), 0 2px 4px -1px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.primaryColor[0]) +
                ", 0.2)"
        },
        "&:hover,&:focus": {
            zIndex: "2",
            cursor: "default"
        }
    },
    info: {
        "&,&:hover,&:focus": {
            backgroundColor: material_kit_pro_react_1.infoColor[0],
            borderColor: material_kit_pro_react_1.infoColor[0],
            color: material_kit_pro_react_1.whiteColor,
            boxShadow: "0 4px 5px 0 rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.infoColor[0]) +
                ", 0.14), 0 1px 10px 0 rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.infoColor[0]) +
                ", 0.12), 0 2px 4px -1px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.infoColor[0]) +
                ", 0.2)"
        },
        "&:hover,&:focus": {
            zIndex: "2",
            cursor: "default"
        }
    },
    success: {
        "&,&:hover,&:focus": {
            backgroundColor: material_kit_pro_react_1.successColor[0],
            borderColor: material_kit_pro_react_1.successColor[0],
            color: material_kit_pro_react_1.whiteColor,
            boxShadow: "0 4px 5px 0 rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.successColor[0]) +
                ", 0.14), 0 1px 10px 0 rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.successColor[0]) +
                ", 0.12), 0 2px 4px -1px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.successColor[0]) +
                ", 0.2)"
        },
        "&:hover,&:focus": {
            zIndex: "2",
            cursor: "default"
        }
    },
    warning: {
        "&,&:hover,&:focus": {
            backgroundColor: material_kit_pro_react_1.warningColor[0],
            borderColor: material_kit_pro_react_1.warningColor[0],
            color: material_kit_pro_react_1.whiteColor,
            boxShadow: "0 4px 5px 0 rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.warningColor[0]) +
                ", 0.14), 0 1px 10px 0 rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.warningColor[0]) +
                ", 0.12), 0 2px 4px -1px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.warningColor[0]) +
                ", 0.2)"
        },
        "&:hover,&:focus": {
            zIndex: "2",
            cursor: "default"
        }
    },
    danger: {
        "&,&:hover,&:focus": {
            backgroundColor: material_kit_pro_react_1.dangerColor[0],
            borderColor: material_kit_pro_react_1.dangerColor[0],
            color: material_kit_pro_react_1.whiteColor,
            boxShadow: "0 4px 5px 0 rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.dangerColor[0]) +
                ", 0.14), 0 1px 10px 0 rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.dangerColor[0]) +
                ", 0.12), 0 2px 4px -1px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.dangerColor[0]) +
                ", 0.2)"
        },
        "&:hover,&:focus": {
            zIndex: "2",
            cursor: "default"
        }
    },
    disabled: {
        "&,&:hover,&:focus": {
            color: material_kit_pro_react_1.grayColor[10],
            cursor: "not-allowed",
            backgroundColor: material_kit_pro_react_1.whiteColor,
            borderColor: material_kit_pro_react_1.grayColor[6]
        }
    }
});
exports.default = paginationStyle;
//# sourceMappingURL=paginationStyle.js.map