"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@material-ui/core");
const material_kit_pro_react_1 = require("../../material-kit-pro-react");
const parallaxStyle = core_1.createStyles({
    parallax: {
        height: "100vh",
        maxHeight: "1600px",
        overflow: "hidden",
        position: "relative",
        backgroundPosition: "50%",
        backgroundSize: "cover",
        margin: "0",
        padding: "0",
        border: "0",
        display: "flex",
        alignItems: "center"
    },
    filter: {},
    primaryColor: {
        "&:before": {
            background: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) + ", 0.5)"
        },
        "&:after": {
            background: "linear-gradient(60deg,rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.primaryColor[4]) +
                ",.56),rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.primaryColor[5]) +
                ",.95))"
        },
        "&:after,&:before": {
            position: "absolute",
            zIndex: "1",
            width: "100%",
            height: "100%",
            display: "block",
            left: "0",
            top: "0",
            content: "''"
        }
    },
    roseColor: {
        "&:before": {
            background: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) + ", 0.5)"
        },
        "&:after": {
            background: "linear-gradient(60deg,rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.roseColor[3]) +
                ",.56),rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.roseColor[4]) +
                ",.95))"
        },
        "&:after,&:before": {
            position: "absolute",
            zIndex: "1",
            width: "100%",
            height: "100%",
            display: "block",
            left: "0",
            top: "0",
            content: "''"
        }
    },
    darkColor: {
        "&:before": {
            background: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) + ", 0.5)"
        },
        "&:after,&:before": {
            position: "absolute",
            zIndex: "1",
            width: "100%",
            height: "100%",
            display: "block",
            left: "0",
            top: "0",
            content: "''"
        }
    },
    infoColor: {
        "&:before": {
            background: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) + ", 0.5)"
        },
        "&:after": {
            background: "linear-gradient(60deg,rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.infoColor[6]) +
                ",.56),rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.infoColor[7]) +
                ",.95))"
        },
        "&:after,&:before": {
            position: "absolute",
            zIndex: "1",
            width: "100%",
            height: "100%",
            display: "block",
            left: "0",
            top: "0",
            content: "''"
        }
    },
    successColor: {
        "&:before": {
            background: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) + ", 0.5)"
        },
        "&:after": {
            background: "linear-gradient(60deg,rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.successColor[6]) +
                ",.56),rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.successColor[7]) +
                ",.95))"
        },
        "&:after,&:before": {
            position: "absolute",
            zIndex: "1",
            width: "100%",
            height: "100%",
            display: "block",
            left: "0",
            top: "0",
            content: "''"
        }
    },
    warningColor: {
        "&:before": {
            background: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) + ", 0.5)"
        },
        "&:after": {
            background: "linear-gradient(60deg,rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.warningColor[6]) +
                ",.56),rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.warningColor[7]) +
                ",.95))"
        },
        "&:after,&:before": {
            position: "absolute",
            zIndex: "1",
            width: "100%",
            height: "100%",
            display: "block",
            left: "0",
            top: "0",
            content: "''"
        }
    },
    dangerColor: {
        "&:before": {
            background: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) + ", 0.5)"
        },
        "&:after": {
            background: "linear-gradient(60deg,rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.dangerColor[6]) +
                ",.56),rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.dangerColor[7]) +
                ",.95))"
        },
        "&:after,&:before": {
            position: "absolute",
            zIndex: "1",
            width: "100%",
            height: "100%",
            display: "block",
            left: "0",
            top: "0",
            content: "''"
        }
    },
    small: {
        height: "65vh",
        minHeight: "65vh",
        maxHeight: "650px"
    }
});
exports.default = parallaxStyle;
//# sourceMappingURL=parallaxStyle.js.map