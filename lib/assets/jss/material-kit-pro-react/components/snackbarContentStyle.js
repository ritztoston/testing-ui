"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const material_kit_pro_react_1 = require("../../material-kit-pro-react");
const core_1 = require("@material-ui/core");
const snackbarContentStyle = core_1.createStyles({
    root: Object.assign(Object.assign({}, material_kit_pro_react_1.defaultFont), { position: "relative", padding: "20px 15px", lineHeight: "20px", marginBottom: "20px", fontSize: "14px", backgroundColor: "white", color: material_kit_pro_react_1.grayColor[15], borderRadius: "0px", maxWidth: "100%", minWidth: "auto", boxShadow: "0 12px 20px -10px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.whiteColor) +
            ", 0.28), 0 4px 20px 0px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
            ", 0.12), 0 7px 8px -5px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.whiteColor) +
            ", 0.2)" }),
    info: Object.assign({ backgroundColor: material_kit_pro_react_1.infoColor[3], color: material_kit_pro_react_1.whiteColor }, material_kit_pro_react_1.infoBoxShadow),
    success: Object.assign({ backgroundColor: material_kit_pro_react_1.successColor[3], color: material_kit_pro_react_1.whiteColor }, material_kit_pro_react_1.successBoxShadow),
    warning: Object.assign({ backgroundColor: material_kit_pro_react_1.warningColor[3], color: material_kit_pro_react_1.whiteColor }, material_kit_pro_react_1.warningBoxShadow),
    danger: Object.assign({ backgroundColor: material_kit_pro_react_1.dangerColor[3], color: material_kit_pro_react_1.whiteColor }, material_kit_pro_react_1.dangerBoxShadow),
    primary: Object.assign({ backgroundColor: material_kit_pro_react_1.primaryColor[3], color: material_kit_pro_react_1.whiteColor }, material_kit_pro_react_1.primaryBoxShadow),
    message: {
        padding: "0",
        display: "block",
        maxWidth: "89%"
    },
    close: {
        width: "20px",
        height: "20px"
    },
    iconButton: {
        width: "24px",
        height: "24px",
        float: "right",
        fontSize: "1.5rem",
        fontWeight: 500,
        lineHeight: "1",
        position: "absolute",
        right: "-4px",
        top: "0",
        padding: "0"
    },
    icon: {
        display: "block",
        float: "left",
        marginRight: "1.071rem"
    },
    container: Object.assign(Object.assign({}, material_kit_pro_react_1.container), { position: "relative" })
});
exports.default = snackbarContentStyle;
//# sourceMappingURL=snackbarContentStyle.js.map