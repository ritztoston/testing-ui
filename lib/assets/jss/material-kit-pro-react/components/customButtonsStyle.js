"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const material_kit_pro_react_1 = require("../../material-kit-pro-react");
const core_1 = require("@material-ui/core");
const buttonStyle = core_1.createStyles({
    button: {
        minHeight: "auto",
        minWidth: "auto",
        backgroundColor: material_kit_pro_react_1.grayColor[0],
        color: material_kit_pro_react_1.whiteColor,
        boxShadow: "0 2px 2px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.grayColor[0]) +
            ", 0.14), 0 3px 1px -2px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.grayColor[0]) +
            ", 0.2), 0 1px 5px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.grayColor[0]) +
            ", 0.12)",
        border: "none",
        borderRadius: "3px",
        position: "relative",
        padding: "12px 30px",
        margin: ".3125rem 1px",
        fontSize: "12px",
        fontWeight: 400,
        textTransform: "uppercase",
        letterSpacing: "0",
        willChange: "box-shadow, transform",
        transition: "box-shadow 0.2s cubic-bezier(0.4, 0, 1, 1), background-color 0.2s cubic-bezier(0.4, 0, 0.2, 1)",
        lineHeight: "1.42857143",
        textAlign: "center",
        whiteSpace: "nowrap",
        verticalAlign: "middle",
        touchAction: "manipulation",
        cursor: "pointer",
        "&:hover,&:focus": {
            color: material_kit_pro_react_1.whiteColor,
            backgroundColor: material_kit_pro_react_1.grayColor[0],
            boxShadow: "0 14px 26px -12px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.grayColor[0]) +
                ", 0.42), 0 4px 23px 0px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
                ", 0.12), 0 8px 10px -5px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.grayColor[0]) +
                ", 0.2)"
        },
        "& .fab,& .fas,& .far,& .fal,& .material-icons": {
            position: "relative",
            display: "inline-block",
            top: "0",
            marginTop: "-1em",
            marginBottom: "-1em",
            fontSize: "1.1rem",
            marginRight: "4px",
            verticalAlign: "middle"
        },
        "& svg": {
            position: "relative",
            display: "inline-block",
            top: "0",
            width: "18px",
            height: "18px",
            marginRight: "4px",
            verticalAlign: "middle"
        },
        "&$justIcon": {
            "& .fab,& .fas,& .far,& .fal,& .material-icons": {
                marginTop: "0px",
                marginRight: "0px",
                position: "absolute",
                width: "100%",
                transform: "none",
                left: "0px",
                top: "0px",
                height: "100%",
                lineHeight: "41px",
                fontSize: "20px"
            }
        }
    },
    fullWidth: {
        width: "100%"
    },
    primary: {
        backgroundColor: material_kit_pro_react_1.primaryColor[0],
        boxShadow: "0 2px 2px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.primaryColor[0]) +
            ", 0.14), 0 3px 1px -2px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.primaryColor[0]) +
            ", 0.2), 0 1px 5px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.primaryColor[0]) +
            ", 0.12)",
        "&:hover,&:focus": {
            backgroundColor: material_kit_pro_react_1.primaryColor[0],
            boxShadow: "0 14px 26px -12px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.primaryColor[0]) +
                ", 0.42), 0 4px 23px 0px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
                ", 0.12), 0 8px 10px -5px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.primaryColor[0]) +
                ", 0.2)"
        }
    },
    secondary: {
        color: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) + ",.87)",
        backgroundColor: material_kit_pro_react_1.secondaryColor[0],
        boxShadow: "0 2px 2px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.secondaryColor[0]) +
            ",.14), 0 3px 1px -2px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.secondaryColor[0]) +
            ",.2), 0 1px 5px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.secondaryColor[0]) +
            ",.12)",
        "&:hover,&:focus": {
            boxShdow: "0 14px 26px -12px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.secondaryColor[0]) +
                ",.42), 0 4px 23px 0 rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
                ",.12), 0 8px 10px -5px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.secondaryColor[0]) +
                ",.2)",
            color: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) + ",.87)",
            backgroundColor: material_kit_pro_react_1.grayColor[19]
        }
    },
    info: {
        backgroundColor: material_kit_pro_react_1.infoColor[0],
        boxShadow: "0 2px 2px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.infoColor[0]) +
            ", 0.14), 0 3px 1px -2px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.infoColor[0]) +
            ", 0.2), 0 1px 5px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.infoColor[0]) +
            ", 0.12)",
        "&:hover,&:focus": {
            backgroundColor: material_kit_pro_react_1.infoColor[0],
            boxShadow: "0 14px 26px -12px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.infoColor[0]) +
                ", 0.42), 0 4px 23px 0px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
                ", 0.12), 0 8px 10px -5px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.infoColor[0]) +
                ", 0.2)"
        }
    },
    success: {
        backgroundColor: material_kit_pro_react_1.successColor[0],
        boxShadow: "0 2px 2px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.successColor[0]) +
            ", 0.14), 0 3px 1px -2px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.successColor[0]) +
            ", 0.2), 0 1px 5px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.successColor[0]) +
            ", 0.12)",
        "&:hover,&:focus": {
            backgroundColor: material_kit_pro_react_1.successColor[0],
            boxShadow: "0 14px 26px -12px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.successColor[0]) +
                ", 0.42), 0 4px 23px 0px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
                ", 0.12), 0 8px 10px -5px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.successColor[0]) +
                ", 0.2)"
        }
    },
    warning: {
        backgroundColor: material_kit_pro_react_1.warningColor[0],
        boxShadow: "0 2px 2px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.warningColor[0]) +
            ", 0.14), 0 3px 1px -2px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.warningColor[0]) +
            ", 0.2), 0 1px 5px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.warningColor[0]) +
            ", 0.12)",
        "&:hover,&:focus": {
            backgroundColor: material_kit_pro_react_1.warningColor[0],
            boxShadow: "0 14px 26px -12px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.warningColor[0]) +
                ", 0.42), 0 4px 23px 0px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
                ", 0.12), 0 8px 10px -5px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.warningColor[0]) +
                ", 0.2)"
        }
    },
    danger: {
        backgroundColor: material_kit_pro_react_1.dangerColor[0],
        boxShadow: "0 2px 2px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.dangerColor[0]) +
            ", 0.14), 0 3px 1px -2px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.dangerColor[0]) +
            ", 0.2), 0 1px 5px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.dangerColor[0]) +
            ", 0.12)",
        "&:hover,&:focus": {
            backgroundColor: material_kit_pro_react_1.dangerColor[0],
            boxShadow: "0 14px 26px -12px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.dangerColor[0]) +
                ", 0.42), 0 4px 23px 0px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
                ", 0.12), 0 8px 10px -5px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.dangerColor[0]) +
                ", 0.2)"
        }
    },
    rose: {
        backgroundColor: material_kit_pro_react_1.roseColor[0],
        boxShadow: "0 2px 2px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.roseColor[0]) +
            ", 0.14), 0 3px 1px -2px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.roseColor[0]) +
            ", 0.2), 0 1px 5px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.roseColor[0]) +
            ", 0.12)",
        "&:hover,&:focus": {
            backgroundColor: material_kit_pro_react_1.roseColor[0],
            boxShadow: "0 14px 26px -12px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.roseColor[0]) +
                ", 0.42), 0 4px 23px 0px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
                ", 0.12), 0 8px 10px -5px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.roseColor[0]) +
                ", 0.2)"
        }
    },
    white: {
        "&,&:focus,&:hover": {
            backgroundColor: material_kit_pro_react_1.whiteColor,
            color: material_kit_pro_react_1.grayColor[0]
        }
    },
    twitter: {
        backgroundColor: material_kit_pro_react_1.twitterColor,
        color: material_kit_pro_react_1.whiteColor,
        boxShadow: "0 2px 2px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.twitterColor) +
            ", 0.14), 0 3px 1px -2px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.twitterColor) +
            ", 0.2), 0 1px 5px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.twitterColor) +
            ", 0.12)",
        "&:hover,&:focus,&:visited": {
            backgroundColor: material_kit_pro_react_1.twitterColor,
            color: material_kit_pro_react_1.whiteColor,
            boxShadow: "0 14px 26px -12px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.twitterColor) +
                ", 0.42), 0 4px 23px 0px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
                ", 0.12), 0 8px 10px -5px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.twitterColor) +
                ", 0.2)"
        }
    },
    facebook: {
        backgroundColor: material_kit_pro_react_1.facebookColor,
        color: material_kit_pro_react_1.whiteColor,
        boxShadow: "0 2px 2px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.facebookColor) +
            ", 0.14), 0 3px 1px -2px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.facebookColor) +
            ", 0.2), 0 1px 5px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.facebookColor) +
            ", 0.12)",
        "&:hover,&:focus": {
            backgroundColor: material_kit_pro_react_1.facebookColor,
            color: material_kit_pro_react_1.whiteColor,
            boxShadow: "0 14px 26px -12px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.facebookColor) +
                ", 0.42), 0 4px 23px 0px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
                ", 0.12), 0 8px 10px -5px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.facebookColor) +
                ", 0.2)"
        }
    },
    google: {
        backgroundColor: material_kit_pro_react_1.googleColor,
        color: material_kit_pro_react_1.whiteColor,
        boxShadow: "0 2px 2px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.googleColor) +
            ", 0.14), 0 3px 1px -2px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.googleColor) +
            ", 0.2), 0 1px 5px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.googleColor) +
            ", 0.12)",
        "&:hover,&:focus": {
            backgroundColor: material_kit_pro_react_1.googleColor,
            color: material_kit_pro_react_1.whiteColor,
            boxShadow: "0 14px 26px -12px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.googleColor) +
                ", 0.42), 0 4px 23px 0px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
                ", 0.12), 0 8px 10px -5px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.googleColor) +
                ", 0.2)"
        }
    },
    linkedin: {
        backgroundColor: material_kit_pro_react_1.linkedinColor,
        color: material_kit_pro_react_1.whiteColor,
        boxShadow: "0 2px 2px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.linkedinColor) +
            ", 0.14), 0 3px 1px -2px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.linkedinColor) +
            ", 0.2), 0 1px 5px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.linkedinColor) +
            ", 0.12)",
        "&:hover,&:focus": {
            backgroundColor: material_kit_pro_react_1.linkedinColor,
            color: material_kit_pro_react_1.whiteColor,
            boxShadow: "0 14px 26px -12px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.linkedinColor) +
                ", 0.42), 0 4px 23px 0px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
                ", 0.12), 0 8px 10px -5px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.linkedinColor) +
                ", 0.2)"
        }
    },
    pinterest: {
        backgroundColor: material_kit_pro_react_1.pinterestColor,
        color: material_kit_pro_react_1.whiteColor,
        boxShadow: "0 2px 2px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.pinterestColor) +
            ", 0.14), 0 3px 1px -2px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.pinterestColor) +
            ", 0.2), 0 1px 5px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.pinterestColor) +
            ", 0.12)",
        "&:hover,&:focus": {
            backgroundColor: material_kit_pro_react_1.pinterestColor,
            color: material_kit_pro_react_1.whiteColor,
            boxShadow: "0 14px 26px -12px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.pinterestColor) +
                ", 0.42), 0 4px 23px 0px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
                ", 0.12), 0 8px 10px -5px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.pinterestColor) +
                ", 0.2)"
        }
    },
    youtube: {
        backgroundColor: material_kit_pro_react_1.youtubeColor,
        color: material_kit_pro_react_1.whiteColor,
        boxShadow: "0 2px 2px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.youtubeColor) +
            ", 0.14), 0 3px 1px -2px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.youtubeColor) +
            ", 0.2), 0 1px 5px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.youtubeColor) +
            ", 0.12)",
        "&:hover,&:focus": {
            backgroundColor: material_kit_pro_react_1.youtubeColor,
            color: material_kit_pro_react_1.whiteColor,
            boxShadow: "0 14px 26px -12px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.youtubeColor) +
                ", 0.42), 0 4px 23px 0px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
                ", 0.12), 0 8px 10px -5px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.youtubeColor) +
                ", 0.2)"
        }
    },
    tumblr: {
        backgroundColor: material_kit_pro_react_1.tumblrColor,
        color: material_kit_pro_react_1.whiteColor,
        boxShadow: "0 2px 2px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.tumblrColor) +
            ", 0.14), 0 3px 1px -2px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.tumblrColor) +
            ", 0.2), 0 1px 5px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.tumblrColor) +
            ", 0.12)",
        "&:hover,&:focus": {
            backgroundColor: material_kit_pro_react_1.tumblrColor,
            color: material_kit_pro_react_1.whiteColor,
            boxShadow: "0 14px 26px -12px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.tumblrColor) +
                ", 0.42), 0 4px 23px 0px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
                ", 0.12), 0 8px 10px -5px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.tumblrColor) +
                ", 0.2)"
        }
    },
    github: {
        backgroundColor: material_kit_pro_react_1.grayColor[8],
        color: material_kit_pro_react_1.whiteColor,
        boxShadow: "0 2px 2px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.grayColor[8]) +
            ", 0.14), 0 3px 1px -2px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.grayColor[8]) +
            ", 0.2), 0 1px 5px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.grayColor[8]) +
            ", 0.12)",
        "&:hover,&:focus": {
            backgroundColor: material_kit_pro_react_1.grayColor[8],
            color: material_kit_pro_react_1.whiteColor,
            boxShadow: "0 14px 26px -12px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.grayColor[8]) +
                ", 0.42), 0 4px 23px 0px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
                ", 0.12), 0 8px 10px -5px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.grayColor[8]) +
                ", 0.2)"
        }
    },
    behance: {
        backgroundColor: material_kit_pro_react_1.behanceColor,
        color: material_kit_pro_react_1.whiteColor,
        boxShadow: "0 2px 2px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.behanceColor) +
            ", 0.14), 0 3px 1px -2px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.behanceColor) +
            ", 0.2), 0 1px 5px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.behanceColor) +
            ", 0.12)",
        "&:hover,&:focus": {
            backgroundColor: material_kit_pro_react_1.behanceColor,
            color: material_kit_pro_react_1.whiteColor,
            boxShadow: "0 14px 26px -12px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.behanceColor) +
                ", 0.42), 0 4px 23px 0px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
                ", 0.12), 0 8px 10px -5px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.behanceColor) +
                ", 0.2)"
        }
    },
    dribbble: {
        backgroundColor: material_kit_pro_react_1.dribbbleColor,
        color: material_kit_pro_react_1.whiteColor,
        boxShadow: "0 2px 2px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.dribbbleColor) +
            ", 0.14), 0 3px 1px -2px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.dribbbleColor) +
            ", 0.2), 0 1px 5px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.dribbbleColor) +
            ", 0.12)",
        "&:hover,&:focus": {
            backgroundColor: material_kit_pro_react_1.dribbbleColor,
            color: material_kit_pro_react_1.whiteColor,
            boxShadow: "0 14px 26px -12px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.dribbbleColor) +
                ", 0.42), 0 4px 23px 0px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
                ", 0.12), 0 8px 10px -5px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.dribbbleColor) +
                ", 0.2)"
        }
    },
    reddit: {
        backgroundColor: material_kit_pro_react_1.redditColor,
        color: material_kit_pro_react_1.whiteColor,
        boxShadow: "0 2px 2px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.redditColor) +
            ", 0.14), 0 3px 1px -2px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.redditColor) +
            ", 0.2), 0 1px 5px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.redditColor) +
            ", 0.12)",
        "&:hover,&:focus": {
            backgroundColor: material_kit_pro_react_1.redditColor,
            color: material_kit_pro_react_1.whiteColor,
            boxShadow: "0 14px 26px -12px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.redditColor) +
                ", 0.42), 0 4px 23px 0px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
                ", 0.12), 0 8px 10px -5px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.redditColor) +
                ", 0.2)"
        }
    },
    instagram: {
        backgroundColor: material_kit_pro_react_1.instagramColor,
        color: material_kit_pro_react_1.whiteColor,
        boxShadow: "0 2px 2px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.instagramColor) +
            ", 0.14), 0 3px 1px -2px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.instagramColor) +
            ", 0.2), 0 1px 5px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.instagramColor) +
            ", 0.12)",
        "&:hover,&:focus": {
            backgroundColor: material_kit_pro_react_1.instagramColor,
            color: material_kit_pro_react_1.whiteColor,
            boxShadow: "0 14px 26px -12px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.instagramColor) +
                ", 0.42), 0 4px 23px 0px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
                ", 0.12), 0 8px 10px -5px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.instagramColor) +
                ", 0.2)"
        }
    },
    simple: {
        "&,&:focus,&:hover": {
            color: material_kit_pro_react_1.whiteColor,
            background: "transparent",
            boxShadow: "none"
        },
        "&$primary": {
            "&,&:focus,&:hover,&:visited": {
                color: material_kit_pro_react_1.primaryColor[0]
            }
        },
        "&$info": {
            "&,&:focus,&:hover,&:visited": {
                color: material_kit_pro_react_1.infoColor[0]
            }
        },
        "&$success": {
            "&,&:focus,&:hover,&:visited": {
                color: material_kit_pro_react_1.successColor[0]
            }
        },
        "&$warning": {
            "&,&:focus,&:hover,&:visited": {
                color: material_kit_pro_react_1.warningColor[0]
            }
        },
        "&$rose": {
            "&,&:focus,&:hover,&:visited": {
                color: material_kit_pro_react_1.roseColor[0]
            }
        },
        "&$danger": {
            "&,&:focus,&:hover,&:visited": {
                color: material_kit_pro_react_1.dangerColor[0]
            }
        },
        "&$twitter": {
            "&,&:focus,&:hover,&:visited": {
                color: material_kit_pro_react_1.twitterColor
            }
        },
        "&$facebook": {
            "&,&:focus,&:hover,&:visited": {
                color: material_kit_pro_react_1.facebookColor
            }
        },
        "&$google": {
            "&,&:focus,&:hover,&:visited": {
                color: material_kit_pro_react_1.googleColor
            }
        },
        "&$linkedin": {
            "&,&:focus,&:hover,&:visited": {
                color: material_kit_pro_react_1.linkedinColor
            }
        },
        "&$pinterest": {
            "&,&:focus,&:hover,&:visited": {
                color: material_kit_pro_react_1.pinterestColor
            }
        },
        "&$youtube": {
            "&,&:focus,&:hover,&:visited": {
                color: material_kit_pro_react_1.youtubeColor
            }
        },
        "&$tumblr": {
            "&,&:focus,&:hover,&:visited": {
                color: material_kit_pro_react_1.tumblrColor
            }
        },
        "&$github": {
            "&,&:focus,&:hover,&:visited": {
                color: material_kit_pro_react_1.grayColor[8]
            }
        },
        "&$behance": {
            "&,&:focus,&:hover,&:visited": {
                color: material_kit_pro_react_1.behanceColor
            }
        },
        "&$dribbble": {
            "&,&:focus,&:hover,&:visited": {
                color: material_kit_pro_react_1.dribbbleColor
            }
        },
        "&$reddit": {
            "&,&:focus,&:hover,&:visited": {
                color: material_kit_pro_react_1.redditColor
            }
        },
        "&$instagram": {
            "&,&:focus,&:hover,&:visited": {
                color: material_kit_pro_react_1.instagramColor
            }
        }
    },
    transparent: {
        "&,&:focus,&:hover": {
            color: "inherit",
            background: "transparent",
            boxShadow: "none"
        }
    },
    disabled: {
        opacity: "0.65",
        pointerEvents: "none"
    },
    lg: {
        "&$justIcon": {
            "& .fab,& .fas,& .far,& .fal,& svg,& .material-icons": {
                marginTop: "-4px"
            }
        },
        padding: "1.125rem 2.25rem",
        fontSize: "0.875rem",
        lineHeight: "1.333333",
        borderRadius: "0.2rem"
    },
    sm: {
        "&$justIcon": {
            "& .fab,& .fas,& .far,& .fal,& svg,& .material-icons": {
                marginTop: "1px"
            }
        },
        padding: "0.40625rem 1.25rem",
        fontSize: "0.6875rem",
        lineHeight: "1.5",
        borderRadius: "0.2rem"
    },
    round: {
        borderRadius: "30px"
    },
    block: {
        width: "100% !important"
    },
    link: {
        "&,&:hover,&:focus": {
            backgroundColor: "transparent",
            color: material_kit_pro_react_1.grayColor[0],
            boxShadow: "none"
        }
    },
    justIcon: {
        paddingLeft: "12px",
        paddingRight: "12px",
        fontSize: "20px",
        height: "41px",
        minWidth: "41px",
        width: "41px",
        "& .fab,& .fas,& .far,& .fal,& svg,& .material-icons": {
            marginRight: "0px"
        },
        "&$lg": {
            height: "57px",
            minWidth: "57px",
            width: "57px",
            lineHeight: "56px",
            "& .fab,& .fas,& .far,& .fal,& .material-icons": {
                fontSize: "32px",
                lineHeight: "56px"
            },
            "& svg": {
                width: "32px",
                height: "32px"
            }
        },
        "&$sm": {
            height: "30px",
            minWidth: "30px",
            width: "30px",
            "& .fab,& .fas,& .far,& .fal,& .material-icons": {
                fontSize: "17px",
                lineHeight: "29px"
            },
            "& svg": {
                width: "17px",
                height: "17px"
            }
        }
    },
    fileButton: {}
});
exports.default = buttonStyle;
//# sourceMappingURL=customButtonsStyle.js.map