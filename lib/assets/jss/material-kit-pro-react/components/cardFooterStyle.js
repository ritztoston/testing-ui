"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@material-ui/core");
const cardFooterStyle = core_1.createStyles({
    cardFooter: {
        display: "flex",
        alignItems: "center",
        backgroundColor: "transparent",
        padding: "0.9375rem 1.875rem",
        paddingTop: "0"
    },
    cardFooterProfile: {
        marginTop: "-15px"
    },
    cardFooterPlain: {
        paddingLeft: "5px",
        paddingRight: "5px",
        backgroundColor: "transparent"
    },
    cardFooterPricing: {
        zIndex: 2
    },
    cardFooterTestimonial: {
        display: "block"
    }
});
exports.default = cardFooterStyle;
//# sourceMappingURL=cardFooterStyle.js.map