"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@material-ui/core");
const material_kit_pro_react_1 = require("../../material-kit-pro-react");
const tableStyle = core_1.createStyles({
    warning: {
        color: material_kit_pro_react_1.warningColor[0]
    },
    primary: {
        color: material_kit_pro_react_1.primaryColor[0]
    },
    danger: {
        color: material_kit_pro_react_1.dangerColor[0]
    },
    success: {
        color: material_kit_pro_react_1.successColor[0]
    },
    info: {
        color: material_kit_pro_react_1.infoColor[0]
    },
    rose: {
        color: material_kit_pro_react_1.roseColor[0]
    },
    gray: {
        color: material_kit_pro_react_1.grayColor[0]
    },
    right: {
        textAlign: "right"
    },
    table: {
        marginBottom: "0",
        width: "100%",
        maxWidth: "100%",
        backgroundColor: "transparent",
        borderSpacing: "0",
        borderCollapse: "collapse",
        overflow: "auto",
        "& > tbody > tr, & > thead > tr": {
            height: "auto"
        }
    },
    tableShoppingHead: {
        fontSize: "0.75em !important",
        textTransform: "uppercase !important"
    },
    tableCell: Object.assign(Object.assign({}, material_kit_pro_react_1.defaultFont), { lineHeight: "1.5em", padding: "12px 8px!important", verticalAlign: "middle", fontSize: "0.875rem", borderBottom: "none", borderTop: "1px solid " + material_kit_pro_react_1.grayColor[6], position: "relative", color: material_kit_pro_react_1.grayColor[1] }),
    tableHeadCell: {
        fontSize: "1.063rem",
        borderBottomWidth: "1px",
        fontWeight: 300,
        color: material_kit_pro_react_1.grayColor[15],
        borderTopWidth: "0 !important"
    },
    tableCellTotal: {
        fontWeight: 500,
        fontSize: "1.0625rem",
        paddingTop: "20px",
        textAlign: "right"
    },
    tableCellAmount: {
        fontSize: "26px",
        fontWeight: 300,
        marginTop: "5px",
        textAlign: "right"
    },
    tableResponsive: {
        minHeight: "0.1%",
        overflowX: "auto"
    },
    tableStripedRow: {
        backgroundColor: material_kit_pro_react_1.grayColor[16]
    },
    tableRowHover: {
        "&:hover": {
            backgroundColor: material_kit_pro_react_1.grayColor[23]
        }
    },
    warningRow: {
        backgroundColor: material_kit_pro_react_1.warningColor[4],
        "&:hover": {
            backgroundColor: material_kit_pro_react_1.warningColor[5]
        }
    },
    dangerRow: {
        backgroundColor: material_kit_pro_react_1.dangerColor[4],
        "&:hover": {
            backgroundColor: material_kit_pro_react_1.dangerColor[5]
        }
    },
    successRow: {
        backgroundColor: material_kit_pro_react_1.successColor[4],
        "&:hover": {
            backgroundColor: material_kit_pro_react_1.successColor[5]
        }
    },
    infoRow: {
        backgroundColor: material_kit_pro_react_1.infoColor[4],
        "&:hover": {
            backgroundColor: material_kit_pro_react_1.infoColor[5]
        }
    }
});
exports.default = tableStyle;
//# sourceMappingURL=tableStyle.js.map