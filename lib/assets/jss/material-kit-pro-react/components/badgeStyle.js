"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const material_kit_pro_react_1 = require("../../material-kit-pro-react");
const core_1 = require("@material-ui/core");
const badgeStyle = core_1.createStyles({
    badge: {
        marginRight: "3px",
        borderRadius: "12px",
        padding: "5px 12px",
        textTransform: "uppercase",
        fontSize: "10px",
        fontWeight: 500,
        lineHeight: "1",
        color: material_kit_pro_react_1.whiteColor,
        textAlign: "center",
        whiteSpace: "nowrap",
        verticalAlign: "baseline",
        display: "inline-block"
    },
    primary: {
        backgroundColor: material_kit_pro_react_1.primaryColor[0]
    },
    warning: {
        backgroundColor: material_kit_pro_react_1.warningColor[0]
    },
    danger: {
        backgroundColor: material_kit_pro_react_1.dangerColor[0]
    },
    success: {
        backgroundColor: material_kit_pro_react_1.successColor[0]
    },
    info: {
        backgroundColor: material_kit_pro_react_1.infoColor[0]
    },
    rose: {
        backgroundColor: material_kit_pro_react_1.roseColor[0]
    },
    gray: {
        backgroundColor: material_kit_pro_react_1.grayColor[7]
    }
});
exports.default = badgeStyle;
//# sourceMappingURL=badgeStyle.js.map