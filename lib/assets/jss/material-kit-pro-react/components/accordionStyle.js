"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const material_kit_pro_react_1 = require("../../material-kit-pro-react");
const core_1 = require("@material-ui/core");
const accordionStyle = (theme) => core_1.createStyles({
    root: {
        flexGrow: 1,
        marginBottom: "20px"
    },
    expansionPanel: {
        boxShadow: "none",
        "&:before": {
            display: "none !important"
        }
    },
    expansionPanelExpanded: {
        margin: "0 !important"
    },
    expansionPanelSummary: {
        minHeight: "auto !important",
        backgroundColor: "transparent",
        borderBottom: "1px solid " + material_kit_pro_react_1.grayColor[6],
        padding: "25px 10px 5px 0px",
        borderTopLeftRadius: "3px",
        borderTopRightRadius: "3px",
        color: material_kit_pro_react_1.grayColor[1]
    },
    primaryExpansionPanelSummary: {
        "&:hover": {
            color: material_kit_pro_react_1.primaryColor[0]
        }
    },
    secondaryExpansionPanelSummary: {
        "&:hover": {
            color: material_kit_pro_react_1.secondaryColor[0]
        }
    },
    warningExpansionPanelSummary: {
        "&:hover": {
            color: material_kit_pro_react_1.warningColor[0]
        }
    },
    dangerExpansionPanelSummary: {
        "&:hover": {
            color: material_kit_pro_react_1.dangerColor[0]
        }
    },
    successExpansionPanelSummary: {
        "&:hover": {
            color: material_kit_pro_react_1.successColor[0]
        }
    },
    infoExpansionPanelSummary: {
        "&:hover": {
            color: material_kit_pro_react_1.infoColor[0]
        }
    },
    roseExpansionPanelSummary: {
        "&:hover": {
            color: material_kit_pro_react_1.roseColor[0]
        }
    },
    expansionPanelSummaryExpaned: {
        "& $expansionPanelSummaryExpandIcon": {
            [theme.breakpoints.up("md")]: {
                top: "auto !important"
            },
            transform: "rotate(180deg)",
            [theme.breakpoints.down("sm")]: {
                top: "10px !important"
            },
            "@media all and (-ms-high-contrast: none), (-ms-high-contrast: active)": {
                display: "inline-block !important",
                top: "10px !important"
            }
        }
    },
    primaryExpansionPanelSummaryExpaned: {
        color: material_kit_pro_react_1.primaryColor[0]
    },
    secondaryExpansionPanelSummaryExpaned: {
        color: material_kit_pro_react_1.secondaryColor[0]
    },
    warningExpansionPanelSummaryExpaned: {
        color: material_kit_pro_react_1.warningColor[0]
    },
    dangerExpansionPanelSummaryExpaned: {
        color: material_kit_pro_react_1.dangerColor[0]
    },
    successExpansionPanelSummaryExpaned: {
        color: material_kit_pro_react_1.successColor[0]
    },
    infoExpansionPanelSummaryExpaned: {
        color: material_kit_pro_react_1.infoColor[0]
    },
    roseExpansionPanelSummaryExpaned: {
        color: material_kit_pro_react_1.roseColor[0]
    },
    expansionPanelSummaryContent: {
        margin: "0 !important"
    },
    expansionPanelSummaryExpandIcon: {
        [theme.breakpoints.up("md")]: {
            top: "auto !important"
        },
        transform: "rotate(0deg)",
        color: "inherit",
        right: "10px",
        position: "absolute",
        [theme.breakpoints.down("sm")]: {
            top: "10px !important"
        },
        "@media all and (-ms-high-contrast: none), (-ms-high-contrast: active)": {
            display: "inline-block !important"
        }
    },
    expansionPanelSummaryExpandIconExpanded: {},
    title: {
        fontSize: "15px",
        fontWeight: "bolder",
        marginTop: "0",
        marginBottom: "0",
        color: "inherit"
    },
    expansionPanelDetails: {
        display: "block",
        padding: "15px 0px 5px",
        fontSize: ".875rem"
    }
});
exports.default = accordionStyle;
//# sourceMappingURL=accordionStyle.js.map