"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@material-ui/core");
const material_kit_pro_react_1 = require("../../material-kit-pro-react");
const typographyStyle = core_1.createStyles({
    defaultFontStyle: Object.assign(Object.assign({}, material_kit_pro_react_1.defaultFont), { fontSize: "14px" }),
    defaultHeaderMargins: {
        marginTop: "20px",
        marginBottom: "10px"
    },
    quote: {
        padding: "10px 20px",
        margin: "0 0 20px",
        fontSize: "1.25rem",
        borderLeft: "5px solid " + material_kit_pro_react_1.grayColor[2]
    },
    quoteText: {
        margin: "0 0 10px",
        fontStyle: "italic"
    },
    quoteAuthor: {
        display: "block",
        fontSize: "80%",
        lineHeight: "1.42857143",
        color: material_kit_pro_react_1.grayColor[10]
    },
    mutedText: {
        "&, & *": {
            color: material_kit_pro_react_1.grayColor[7],
            display: "inline-block"
        }
    },
    primaryText: {
        "&, & *": {
            color: material_kit_pro_react_1.primaryColor[0],
            display: "inline-block"
        }
    },
    infoText: {
        "&, & *": {
            color: material_kit_pro_react_1.infoColor[0],
            display: "inline-block"
        }
    },
    successText: {
        "&, & *": {
            color: material_kit_pro_react_1.successColor[0],
            display: "inline-block"
        }
    },
    warningText: {
        "&, & *": {
            color: material_kit_pro_react_1.warningColor[0],
            display: "inline-block"
        }
    },
    dangerText: {
        "&, & *": {
            color: material_kit_pro_react_1.dangerColor[0],
            display: "inline-block"
        }
    },
    roseText: {
        "&, & *": {
            color: material_kit_pro_react_1.roseColor[0],
            display: "inline-block"
        }
    },
    smallText: {
        fontSize: "65%",
        fontWeight: 400,
        lineHeight: "1",
        color: material_kit_pro_react_1.grayColor[10]
    }
});
exports.default = typographyStyle;
//# sourceMappingURL=typographyStyle.js.map