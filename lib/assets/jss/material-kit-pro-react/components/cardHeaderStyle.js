"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const material_kit_pro_react_1 = require("../../material-kit-pro-react");
const core_1 = require("@material-ui/core");
const cardHeaderStyle = core_1.createStyles({
    cardHeader: {
        borderRadius: "3px",
        padding: "1rem 15px",
        marginLeft: "15px",
        marginRight: "15px",
        marginTop: "-30px",
        border: "0",
        marginBottom: "0"
    },
    cardHeaderPlain: {
        marginLeft: "0px",
        marginRight: "0px",
        "&$cardHeaderImage": {
            margin: "0 !important"
        }
    },
    cardHeaderImage: {
        position: "relative",
        padding: "0",
        zIndex: 1,
        marginLeft: "15px",
        marginRight: "15px",
        marginTop: "-30px",
        borderRadius: "6px",
        "& img": {
            width: "100%",
            borderRadius: "6px",
            pointerEvents: "none",
            boxShadow: "0 5px 15px -8px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
                ", 0.24), 0 8px 10px -5px rgba(" +
                material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
                ", 0.2)"
        },
        "& a": {
            display: "block"
        }
    },
    noShadow: {
        "& img": {
            boxShadow: "none !important"
        }
    },
    cardHeaderContact: {
        margin: "0 15px",
        marginTop: "-20px"
    },
    cardHeaderSignup: {
        marginLeft: "20px",
        marginRight: "20px",
        marginTop: "-40px",
        padding: "20px 0",
        width: "100%",
        marginBottom: "15px"
    },
    warningCardHeader: material_kit_pro_react_1.warningCardHeader,
    successCardHeader: material_kit_pro_react_1.successCardHeader,
    dangerCardHeader: material_kit_pro_react_1.dangerCardHeader,
    infoCardHeader: material_kit_pro_react_1.infoCardHeader,
    primaryCardHeader: material_kit_pro_react_1.primaryCardHeader,
    roseCardHeader: material_kit_pro_react_1.roseCardHeader
});
exports.default = cardHeaderStyle;
//# sourceMappingURL=cardHeaderStyle.js.map