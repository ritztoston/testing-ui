"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const material_kit_pro_react_1 = require("../../material-kit-pro-react");
const core_1 = require("@material-ui/core");
const footerStyle = core_1.createStyles({
    left: {
        float: "left!important",
        display: "block"
    },
    right: {
        padding: "15px 0",
        margin: "0",
        float: "right"
    },
    rightLinks: {
        float: "right!important",
        "& ul": {
            marginBottom: 0,
            marginTop: 10,
            padding: 0,
            listStyle: "none",
            height: 38,
            "& li": {
                display: "inline-block"
            }
        },
        "& i": {
            fontSize: "20px"
        }
    },
    footer: {
        padding: "0.9375rem 0",
        textAlign: "center",
        display: "flex",
        zIndex: 2,
        position: "relative",
        "& ul": {
            marginBottom: "0",
            padding: 0,
            listStyle: "none"
        }
    },
    big: {
        padding: "1.875rem 0",
        "& h5, & h4": {
            fontWeight: 700,
            fontFamily: "Roboto Slab,Times New Roman,serif",
            marginBottom: "15px"
        },
        "& p": {
            color: material_kit_pro_react_1.grayColor[0]
        }
    },
    content: {
        textAlign: "left"
    },
    a: {
        color: material_kit_pro_react_1.primaryColor[0],
        textDecoration: "none",
        backgroundColor: "transparent"
    },
    dark: {
        background: "radial-gradient(ellipse at center," +
            material_kit_pro_react_1.grayColor[4] +
            " 0," +
            material_kit_pro_react_1.grayColor[5] +
            " 100%)",
        backgroundSize: "550% 450%",
        color: material_kit_pro_react_1.whiteColor,
        "& p": {
            color: material_kit_pro_react_1.grayColor[0]
        },
        "& i": {
            color: material_kit_pro_react_1.whiteColor
        },
        "& a": {
            color: material_kit_pro_react_1.whiteColor,
            opacity: ".86",
            "&:visited": {
                color: material_kit_pro_react_1.whiteColor
            },
            "&:focus, &:hover": {
                opacity: 1
            }
        },
        "& hr": {
            borderColor: "rgba(" + material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.whiteColor) + ",0.2)"
        },
        "& $btnTwitter, & $btnDribbble, & $btnInstagram": {
            color: material_kit_pro_react_1.whiteColor
        }
    },
    white: {
        backgroundColor: material_kit_pro_react_1.whiteColor,
        color: material_kit_pro_react_1.grayColor[1],
        textDecoration: "none",
        "& a": {
            "&:visited": {
                color: material_kit_pro_react_1.grayColor[1]
            },
            "&:hover, &:focus": {
                color: material_kit_pro_react_1.grayColor[20]
            }
        }
    },
    container: material_kit_pro_react_1.container,
    list: {
        marginBottom: "0",
        padding: "0",
        marginTop: "0"
    },
    inlineBlock: {
        display: "inline-block",
        padding: "0px",
        width: "auto"
    },
    icon: {
        width: "18px",
        height: "18px",
        position: "relative",
        top: "3px"
    },
    iconSocial: {
        width: "41px",
        height: "41px",
        fontSize: "24px",
        minWidth: "41px",
        padding: 0,
        overflow: "hidden",
        position: "relative"
    },
    btnTwitter: Object.assign(Object.assign({}, material_kit_pro_react_1.btnLink), { color: material_kit_pro_react_1.twitterColor }),
    btnDribbble: Object.assign(Object.assign({}, material_kit_pro_react_1.btnLink), { color: material_kit_pro_react_1.dribbbleColor }),
    btnInstagram: Object.assign(Object.assign({}, material_kit_pro_react_1.btnLink), { color: material_kit_pro_react_1.instagramColor }),
    footerBrand: {
        height: "50px",
        padding: "15px 15px",
        fontSize: "18px",
        lineHeight: "50px",
        marginLeft: "-15px",
        color: material_kit_pro_react_1.grayColor[1],
        textDecoration: "none",
        fontWeight: 700,
        fontFamily: "Roboto Slab,Times New Roman,serif"
    },
    pullCenter: {
        display: "inline-block",
        float: "none"
    },
    clearFix: {
        clear: "both"
    }
});
exports.default = footerStyle;
//# sourceMappingURL=footerStyle.js.map