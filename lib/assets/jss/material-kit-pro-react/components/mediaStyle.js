"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const material_kit_pro_react_1 = require("../../material-kit-pro-react");
const core_1 = require("@material-ui/core");
const mediaStyle = core_1.createStyles({
    media: {
        display: "flex",
        WebkitBoxAlign: "start",
        alignItems: "flex-start",
        "& p": {
            color: material_kit_pro_react_1.grayColor[0],
            fontSize: "1rem",
            lineHeight: "1.6em"
        },
        "& $media $mediaBody": {
            paddingRight: "0px"
        }
    },
    mediaLink: {
        padding: "10px",
        float: "left !important"
    },
    mediaAvatar: {
        margin: "0 auto",
        width: "64px",
        height: "64px",
        overflow: "hidden",
        borderRadius: "50%",
        marginRight: "15px",
        boxShadow: "0 6px 10px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
            ", 0.14), 0 1px 18px 0 rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
            ", 0.12), 0 3px 5px -1px rgba(" +
            material_kit_pro_react_1.hexToRgb(material_kit_pro_react_1.blackColor) +
            ", 0.2)",
        "& img": {
            width: "100%"
        }
    },
    mediaBody: {
        paddingRight: "10px",
        WebkitBoxFlex: 1,
        flex: "1"
    },
    mediaHeading: Object.assign(Object.assign({}, material_kit_pro_react_1.title), { marginTop: "10px", marginBottom: "10px", "& small": {
            fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif'
        } }),
    mediaFooter: {
        "& button, & a": {
            marginBottom: "20px"
        },
        "&:after": {
            display: "table",
            content: '" "',
            clear: "both"
        }
    }
});
exports.default = mediaStyle;
//# sourceMappingURL=mediaStyle.js.map