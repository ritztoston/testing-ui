"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@material-ui/core");
const customFileInputStyle = core_1.createStyles({
    inputFile: {
        opacity: "0",
        position: "absolute",
        top: "0",
        right: "0",
        bottom: "0",
        left: "0",
        width: "100%",
        height: "100%",
        zIndex: -1
    },
    inputFileWrapper: {
        "& button svg": {
            color: "inherit"
        }
    }
});
exports.default = customFileInputStyle;
//# sourceMappingURL=customFileInputStyle.js.map