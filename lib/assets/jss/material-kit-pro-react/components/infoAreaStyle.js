"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const material_kit_pro_react_1 = require("../../material-kit-pro-react");
const core_1 = require("@material-ui/core");
const infoStyle = core_1.createStyles({
    infoArea: {
        maxWidth: "360px",
        margin: "0 auto",
        padding: "70px 0 30px"
    },
    iconWrapper: {
        float: "left",
        marginTop: "24px",
        marginRight: "10px"
    },
    primary: {
        color: material_kit_pro_react_1.primaryColor[0]
    },
    warning: {
        color: material_kit_pro_react_1.warningColor[0]
    },
    danger: {
        color: material_kit_pro_react_1.dangerColor[0]
    },
    success: {
        color: material_kit_pro_react_1.successColor[0]
    },
    info: {
        color: material_kit_pro_react_1.infoColor[0]
    },
    rose: {
        color: material_kit_pro_react_1.roseColor[0]
    },
    gray: {
        color: material_kit_pro_react_1.grayColor[0]
    },
    icon: {
        width: "2.25rem",
        height: "2.25rem",
        fontSize: "2.25rem"
    },
    descriptionWrapper: {
        color: material_kit_pro_react_1.grayColor[0],
        overflow: "hidden"
    },
    title: Object.assign(Object.assign({}, material_kit_pro_react_1.title), { margin: "1.75rem 0 0.875rem !important", minHeight: "unset" }),
    description: {
        color: material_kit_pro_react_1.grayColor[0],
        overflow: "hidden",
        marginTop: "0px",
        "& p": {
            color: material_kit_pro_react_1.grayColor[0],
            fontSize: "14px"
        }
    },
    iconWrapperVertical: {
        float: "none"
    },
    iconVertical: {
        width: "61px",
        height: "61px"
    }
});
exports.default = infoStyle;
//# sourceMappingURL=infoAreaStyle.js.map