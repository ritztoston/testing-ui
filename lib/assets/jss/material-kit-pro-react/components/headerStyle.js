"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@material-ui/core");
const material_kit_pro_react_1 = require("../../material-kit-pro-react");
const headerStyle = (theme) => core_1.createStyles({
    appBar: {
        display: "flex",
        border: "0",
        borderRadius: "3px",
        padding: "0.625rem 0",
        marginBottom: "20px",
        color: material_kit_pro_react_1.grayColor[15],
        width: "100%",
        backgroundColor: material_kit_pro_react_1.whiteColor,
        boxShadow: "0 4px 18px 0px rgba(" +
            core_1.hexToRgb(material_kit_pro_react_1.blackColor) +
            ", 0.12), 0 7px 10px -5px rgba(" +
            core_1.hexToRgb(material_kit_pro_react_1.blackColor) +
            ", 0.15)",
        transition: "all 150ms ease 0s",
        alignItems: "center",
        flexFlow: "row nowrap",
        justifyContent: "flex-start",
        position: "relative"
    },
    absolute: {
        position: "absolute",
        top: "auto"
    },
    fixed: {
        position: "fixed"
    },
    container: Object.assign(Object.assign({}, material_kit_pro_react_1.container), { minHeight: "50px", alignItems: "center", justifyContent: "space-between", display: "flex", flexWrap: "nowrap" }),
    title: {
        letterSpacing: "unset",
        "&,& a": Object.assign(Object.assign({}, material_kit_pro_react_1.defaultFont), { minWidth: "unset", lineHeight: "30px", fontSize: "18px", borderRadius: "3px", textTransform: "none", whiteSpace: "nowrap", color: "inherit", "&:hover,&:focus": {
                color: "inherit",
                background: "transparent"
            } })
    },
    appResponsive: {
        margin: "20px 10px",
        marginTop: "0px"
    },
    primary: {
        backgroundColor: material_kit_pro_react_1.primaryColor[0],
        color: material_kit_pro_react_1.whiteColor,
        boxShadow: "0 4px 20px 0px rgba(" +
            core_1.hexToRgb(material_kit_pro_react_1.blackColor) +
            ", 0.14), 0 7px 12px -5px rgba(" +
            core_1.hexToRgb(material_kit_pro_react_1.primaryColor[0]) +
            ", 0.46)"
    },
    info: {
        backgroundColor: material_kit_pro_react_1.infoColor[0],
        color: material_kit_pro_react_1.whiteColor,
        boxShadow: "0 4px 20px 0px rgba(" +
            core_1.hexToRgb(material_kit_pro_react_1.blackColor) +
            ", 0.14), 0 7px 12px -5px rgba(" +
            core_1.hexToRgb(material_kit_pro_react_1.infoColor[0]) +
            ", 0.46)"
    },
    success: {
        backgroundColor: material_kit_pro_react_1.successColor[0],
        color: material_kit_pro_react_1.whiteColor,
        boxShadow: "0 4px 20px 0px rgba(" +
            core_1.hexToRgb(material_kit_pro_react_1.blackColor) +
            ", 0.14), 0 7px 12px -5px rgba(" +
            core_1.hexToRgb(material_kit_pro_react_1.successColor[0]) +
            ", 0.46)"
    },
    warning: {
        backgroundColor: material_kit_pro_react_1.warningColor[0],
        color: material_kit_pro_react_1.whiteColor,
        boxShadow: "0 4px 20px 0px rgba(" +
            core_1.hexToRgb(material_kit_pro_react_1.blackColor) +
            ", 0.14), 0 7px 12px -5px rgba(" +
            core_1.hexToRgb(material_kit_pro_react_1.warningColor[0]) +
            ", 0.46)"
    },
    danger: {
        backgroundColor: material_kit_pro_react_1.dangerColor[0],
        color: material_kit_pro_react_1.whiteColor,
        boxShadow: "0 4px 20px 0px rgba(" +
            core_1.hexToRgb(material_kit_pro_react_1.blackColor) +
            ", 0.14), 0 7px 12px -5px rgba(" +
            core_1.hexToRgb(material_kit_pro_react_1.dangerColor[0]) +
            ", 0.46)"
    },
    rose: {
        backgroundColor: material_kit_pro_react_1.roseColor[0],
        color: material_kit_pro_react_1.whiteColor,
        boxShadow: "0 4px 20px 0px rgba(" +
            core_1.hexToRgb(material_kit_pro_react_1.blackColor) +
            ", 0.14), 0 7px 12px -5px rgba(" +
            core_1.hexToRgb(material_kit_pro_react_1.roseColor[0]) +
            ", 0.46)"
    },
    transparent: {
        backgroundColor: "transparent !important",
        boxShadow: "none",
        paddingTop: "25px",
        color: material_kit_pro_react_1.whiteColor
    },
    dark: {
        color: material_kit_pro_react_1.whiteColor,
        backgroundColor: material_kit_pro_react_1.grayColor[9] + " !important",
        boxShadow: "0 4px 20px 0px rgba(" +
            core_1.hexToRgb(material_kit_pro_react_1.blackColor) +
            ", 0.14), 0 7px 12px -5px rgba(" +
            core_1.hexToRgb(material_kit_pro_react_1.grayColor[9]) +
            ", 0.46)"
    },
    white: {
        border: "0",
        padding: "0.625rem 0",
        marginBottom: "20px",
        color: material_kit_pro_react_1.grayColor[15],
        backgroundColor: material_kit_pro_react_1.whiteColor + " !important",
        boxShadow: "0 4px 18px 0px rgba(" +
            core_1.hexToRgb(material_kit_pro_react_1.blackColor) +
            ", 0.12), 0 7px 10px -5px rgba(" +
            core_1.hexToRgb(material_kit_pro_react_1.blackColor) +
            ", 0.15)"
    },
    drawerPaper: Object.assign(Object.assign(Object.assign({ border: "none", bottom: "0", transitionProperty: "top, bottom, width", transitionDuration: ".2s, .2s, .35s", transitionTimingFunction: "linear, linear, ease", width: material_kit_pro_react_1.drawerWidth }, material_kit_pro_react_1.boxShadow), { position: "fixed", display: "block", top: "0", height: "100vh", right: "0", left: "auto", visibility: "visible", overflowY: "visible", borderTop: "none", textAlign: "left", paddingRight: "0px", paddingLeft: "0" }), material_kit_pro_react_1.transition),
    hidden: {
        width: "100%"
    },
    collapse: {
        [theme.breakpoints.up("md")]: {
            display: "flex !important",
            MsFlexPreferredSize: "auto",
            flexBasis: "auto"
        },
        WebkitBoxFlex: 1,
        MsFlexPositive: "1",
        flexGrow: 1,
        WebkitBoxAlign: "center",
        MsFlexAlign: "center",
        alignItems: "center"
    },
    closeButtonDrawer: {
        position: "absolute",
        right: "8px",
        top: "9px",
        zIndex: 1
    }
});
exports.default = headerStyle;
//# sourceMappingURL=headerStyle.js.map